// import external dependencies
import 'jquery';

// Import everything from autoload
import './autoload/**/*'

// import local dependencies
import Router from './util/Router';
import common from './routes/common';
import home from './routes/home';
import aboutUs from './routes/about';
import singleEspacio from './routes/single-espacio';
import singleTerritorio from './routes/single-territorio';
import noticias from './routes/noticias';
import postTypeArchiveEvento from './routes/eventos';
import merlinHubApp from './routes/merlin-hub-app';
import templateEventos from './routes/template-eventos';

/** Populate Router instance with DOM routes */
const routes = new Router({
  // All pages
  common,
  // Home page
  home,
  // About Us page, note the change from about-us to aboutUs.
  aboutUs,
  singleEspacio,
  singleTerritorio,
  noticias,
  postTypeArchiveEvento,
  merlinHubApp,
  templateEventos,
});

// Load Events
jQuery(document).ready(() => routes.loadEvents());
