export default {
  init() {
    let offset = $('.badges').offset();
    let paso = offset.top - 50;
    var ventana_ancho = $(window).width();
    $(window).scroll(function(){
      let windowTop = $(document).scrollTop();
      if ( windowTop > paso && ventana_ancho > 768 ) {
        $('.badges').addClass('fixedBadges');
      } else {
        $('.badges').removeClass('fixedBadges');
      }
    });

  },
  finalize() {
    // JavaScript to be fired on all pages, after page specific JS is fired
  },
};
