export default {
  init() {
    // JavaScript to be fired on all pages

    $('.hamburger').click(function () {
      $(this).toggleClass('is-active');
      $('#contMenu').toggle();
      $('#brandPrincipal').toggle();
      $('ul.listaRedes').toggleClass('redesInline');
      // $('.banner').toggleClass('estirado');
      $('.wpml-ls-legacy-list-vertical>ul').toggleClass('banderasInline');
      $('.redesHeader').toggleClass('moverRedes');
      $('.banderasHeader').toggleClass('moverBanderas');


    });
    // JavaScript to be fired on all pages
    $('.dropdown-menu').wrap('<div class="submenuCapa"></div>');

  },
  finalize() {
    // JavaScript to be fired on all pages, after page specific JS is fired
  },
};
