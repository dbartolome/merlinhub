var InfiniteScroll = require('infinite-scroll');
// var imagesLoaded = require('imagesloaded');

export default {
  init() {

    // infinite-scroll
    // -----------------------------------------------

    let buttonCont = $('.button-container');

    let main = new InfiniteScroll( '.infinite-scroll-container', {
      path: '.next-link a',
      append: '.art',
      history: false,
      hideNav: '.nav-links',
      button: '.view-more-button',
      status: '.page-load-status',
      debug: true,
    });

    function onPageLoad() {
      console.log(main.loadCount);
      console.log('main.loadCount');
      if ( main.loadCount == 1 ) {
        main.option({
          loadOnScroll: false,
        });
        buttonCont.removeClass('d-none');
        main.off( 'load', onPageLoad );
      }
    }

    main.on( 'load', onPageLoad );

    main.on( 'last', function() {
      buttonCont.hide();
    });

    // hack para safari
    // https://github.com/metafizzy/infinite-scroll/issues/770#issuecomment-374600629

    main.on( 'append', function( response, path, items ) {
      for ( var i=0; i < items.length; i++ ) {
        reloadSrcsetImgs( items[i] );
      }
    });

    function reloadSrcsetImgs( items ) {
      var imgs = items.querySelectorAll('img[srcset]');
      for ( var i=0; i < imgs.length; i++ ) {
        var img = imgs[i];
        img.outerHTML = img.outerHTML;
      }
    }

  },
};
