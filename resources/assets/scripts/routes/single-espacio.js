/* eslint-disable no-unused-vars */
import Glide, { Controls, Autoplay, Keyboard } from '@glidejs/glide/dist/glide.modular.esm'

export default {
  init() {

      let glide = new Glide('.glide', {
        type: 'carousel',
        autoplay: 500000,
        gap: 15,
      })
      glide.mount({ Controls, Autoplay, Keyboard });

      // scroll footer
      let footer = $('#footer');
      let footerHeight = footer.outerHeight();
      let galeria = $('.galeria');

      $(window).scroll(function() {
        let height = $(window).scrollTop();
        let punto = $(document).height() - footerHeight - $(window).height();

        // console.log('window: ' + $(window).height());
        // console.log('doc: ' + $(document).height());
        // console.log('top: ' + $(window).scrollTop());
        // console.log('punto: ' + punto);
        // console.log('footerH: ' + footerHeight);

        if(height  < punto) {
            galeria.addClass('fija');
        } else {
          galeria.removeClass('fija');
        }
    });

  },
};
