export default {
  init() {
    console.log('empezamos eventos');
    let varFiltroAnho;
    let varFiltroMes;
    $('#botonesAnho .btn').on('click',function(){
        varFiltroAnho = $(this).attr('id');
        $('.art').css('display','none');
        $('.'+varFiltroAnho).css('display','block');
        $('.meses').css('display','none');
        $('.meses'+varFiltroAnho).css('display','block');
    })

    $('#botonesMes .btn').on('click',function(){
      varFiltroAnho = $(this).data('anho');
      console.log(varFiltroAnho);
      varFiltroMes = $(this).attr('id');
      console.log(varFiltroMes);
      $('.'+varFiltroAnho).css('display','none');
      $('.'+varFiltroAnho+varFiltroMes).css('display','block');
    })

    $('#vertodos').on('click',function(){
      $('.art').fadeIn();

    })

  },
  finalize() {
    // JavaScript to be fired on all pages, after page specific JS is fired
  },
};
