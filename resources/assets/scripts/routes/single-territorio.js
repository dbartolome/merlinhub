import { gsap } from 'gsap';
//import { Flip } from "gsap/Flip";
import { ScrollTrigger } from 'gsap/ScrollTrigger';
import { ScrollToPlugin } from 'gsap/ScrollToPlugin';
import { Draggable } from 'gsap/Draggable';
//import { EaselPlugin } from "gsap/EaselPlugin";
//import { MotionPathPlugin } from "gsap/MotionPathPlugin";
//import { PixiPlugin } from "gsap/PixiPlugin";
//import { TextPlugin } from "gsap/TextPlugin";


export default {
  init() {


    gsap.registerPlugin(ScrollTrigger, ScrollToPlugin, Draggable);

    let sections = gsap.utils.toArray('.box');

    gsap.to(sections,
      {
        xPercent: -100 * (sections.length - 1),
        ease: 'none',
        scrollTrigger: {
          trigger: '.container',
          pin: true,
          scrub: 1,
          snap: 1/(sections.length - 1),
          end: '+=3500',
      },
    });


    $('.detalles').click(function () {
      $('.detallesTxt').fadeOut();
      $('.tituloCirculo').css('fill', '#282270');
      $('.detallesTxt', this).fadeIn();
      $('.tituloCirculo', this).css('fill', '#13DB00');
    });


    let misContenedores = document.querySelectorAll('.box');

    let contFinal = 0;
    for (let i = 0; i < misContenedores.length; i++) {
      let contador = misContenedores[i].style.width;
      contador = parseInt(contador.substring(0,2));
      contFinal = contFinal + contador;

    }
    let tamanoPX = ((contFinal * window.innerWidth) / 100) - window.innerWidth;
    let misParallax = document.querySelectorAll('.parallax');




    let ventanaAncho = $(window).width();

    $(window).scroll(function () {
      let windowLeft = $(document).scrollLeft();
      let windowFinal = (window.innerHeight/tamanoPX) * windowLeft;
      $('.barraLateral').css('height', windowFinal+'px');




      if(misParallax.length > 0 && ventanaAncho > 992) {
        var i = 1;
        $('.imgFondo').each(function () {
          var generandoMov = $('.parallax' + i).offset();
          var movReal = generandoMov.left - $(window).width();
          var movimiento = ($(window).scrollLeft() - movReal) / $('.parallax' + i).data('movimiento');

          $(this).css({
            'right': '-' + movimiento + 'px',
          });
          i++;
        });
      }
    });

    $('#arrowAnim').on('click', function () {
      console.log('pincho en las flechas');
      $('html, body, *').scrollLeft(500);
    });

  },
};
