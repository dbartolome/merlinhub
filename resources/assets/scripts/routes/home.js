
export default {
  init() {

    $( document ).ready(function() {
      let ventana_ancho = $(window).width();
      let anchoMenu = $('#contMenu').innerWidth();
      console.log(anchoMenu);
      if(ventana_ancho > 992) {
        $('.hamburger').toggleClass('is-active');
        $('#contMenu').toggle();
        $('#brandPrincipal').toggle();
        $('ul.listaRedes').addClass('redesInline');
        $('.wpml-ls-legacy-list-vertical>ul').addClass('banderasInline');
        $('.redesHeader').addClass('moverRedes');
        $('.banderasHeader').addClass('moverBanderas');
        $('.capaTexto').css('margin-left','150px');
        $('.btnAbrir').css('margin-left','150px');
        $('.btnAbrirEn').css('margin-left','150px');
      }
    });

    //var ventana_ancho = $(window).width();
    let cont = 1;
    $(window).scroll(function(){
      let ventana_ancho = $(window).width();
      let windowTop = $(document).scrollTop();
      if(ventana_ancho > 992) {
        if (windowTop > 150 && cont == 1) {
          $('.hamburger').removeClass('is-active');
          $('#contMenu').fadeOut();
          $('#brandPrincipal').fadeIn();
          $('ul.listaRedes').removeClass('redesInline');
          $('.wpml-ls-legacy-list-vertical>ul').removeClass('banderasInline');
          $('.redesHeader').removeClass('moverRedes');
          $('.banderasHeader').removeClass('moverBanderas');
          $('.capaTexto').css('margin-left','0');
          $('.btnAbrir').css('margin-left','0');
          $('.btnAbrirEn').css('margin-left','0');
          cont = 2;
          console.log('esto es un nuevo paso ' + cont);
        } else if (windowTop == 0 && cont == 2) {
          $('.hamburger').toggleClass('is-active');
          $('#contMenu').toggle();
          $('#brandPrincipal').toggle();
          $('ul.listaRedes').addClass('redesInline');
          $('.wpml-ls-legacy-list-vertical>ul').addClass('banderasInline');
          $('.redesHeader').addClass('moverRedes');
          $('.banderasHeader').addClass('moverBanderas');
          $('.capaTexto').css('margin-left','150px');
          $('.btnAbrir').css('margin-left','150px');
          $('.btnAbrirEn').css('margin-left','150px');
          cont = 1;
        }
      }
    });

    // JavaScript to be fired on the home page
    $('.espacios').mouseover(function(){
      $('.txt',this).css({
        'fill':'#282270',
        'font-weight' : '900',

      });
    });
    $('.espacios').mouseout(function(){
      $('.txt',this).css({
        'fill': 'rgba(0,0,0,0.4)',
        'font-weight': 'normal',
      });
    });

    $('.espacios').click(function() {
      let url = $(this).data('url');
      window.open(url, '_self');
      return false;
    });



    $('.btnAbrir').on('click', function(){
      $('body').append('<div class="miNuevCapa" style="z-index: 99999; width: 100%; height: 100%; background-color: #0a0a0a; top: 0; left: 0; position: fixed;"><div class="btnEliminarCapa" id="capaVideo" style= "display: block; position: fixed; z-index: 99999999; top: 30px; right: 30px"><img src="/wp-content/themes/merlinhub/dist/images/closeicon.png" width="30px"></div>' +
        '<video width="100%" height="100%" controls autoplay>\n' +
        '  <source src="https://merlinhub.es/video/merlinhub_grande.mp4" type="video/mp4">\n' +
        'Your browser does not support the video tag.\n' +
        '</video></div>');
    });

    $('.btnAbrirEn').on('click', function(){
      $('body').append('<div class="miNuevCapa" style="z-index: 99999; width: 100%; height: 100%; background-color: #0a0a0a; top: 0; left: 0; position: fixed;"><div class="btnEliminarCapa" id="capaVideo" style= "display: block; position: fixed; z-index: 99999999; top: 30px; right: 30px"><img src="/wp-content/themes/merlinhub/dist/images/closeicon.png" width="30px"></div>' +
        '<video width="100%" height="100%" controls autoplay>\n' +
        '  <source src="https://merlinhub.es/video/video_merlinhub_EN.mp4" type="video/mp4">\n' +
        'Your browser does not support the video tag.\n' +
        '</video></div>');
    });

    $('body').delegate('.btnEliminarCapa', 'click',function(){
     $('.miNuevCapa').remove();
    });


    $('.rewindClass').click(function(){
      $('video').prop('currentTime',0);
      $('video').trigger('play');
    });

    $('.rewindClassDos').click(function(){
      console.log('voy  quitar el volumen');
      $('video').prop('currentTime',0);
      $('video').prop('muted', false);
    });

    $('.rewindClassTres').click(function(){
      $('video').prop('currentTime',0);
      $('video').prop('muted', true);
    });

    $('.upVolumen').click(function() {
      var volume = $('video').prop('volume') + 0.2;
      if (volume > 1) {
        volume = 1;
      }
      $('video').prop('volume', volume);
    });
/*
    $('.quitarVol').click(function() {
      console.log('ocultar los controles de volumen');
      $('.home .controlVolumen').css('display','none')
    });

    $('.ponerVol').click(function() {
      console.log('mostrar los controles de volumen');
      $('.home .controlVolumen').css('display','block')
    });*/

    $('.downVolumen').click(function() {
      var volume = $('video').prop('volume')-0.2;
      if(volume <0){
        volume = 0;
      }
      $('video').prop('volume',volume);
    });


  },
  finalize() {
    // JavaScript to be fired on the home page, after the init JS
  },
};
