{{--
  Template Name: Template eventos anteriores
--}}

@extends('layouts.app')

@section('content')
  @include('partials.page-header-evanteriores')
          <?php
            $introRepoEventos = get_field('introSeccionEventosRepo', 'option');
          $restoArticulos = '';
              $args = array(
                  'post_type'=>'evento',
                  'posts_per_archive_page' => -1,
                  'meta_key'  => 'fechaEvento',
                   'orderby'   => 'meta_value_num',
                    'order'     => 'DESC',
                 );
              $loop = new WP_Query($args);

              $i = 0;
              $anhoCrear = 0;
              $botonesAnho = array();
              $botonesMes = array();
              $varAnhoComp = 0;
              $varMesComp = 'Enero';
              $i=0;
              $j=0;
              $k =0;

              while ($loop -> have_posts()) {
                $loop ->the_post();

                $unixtimestamp = strtotime( get_field('fechaEvento') );
                $anho = intval(date_i18n( "Y", $unixtimestamp ));
                $mes = date_i18n( "F", $unixtimestamp );
                $dia = intval(date_i18n( "d", $unixtimestamp ));

                if($varAnhoComp == $anho) {

                } else {
                  $botonesAnho[$i] = $anho;
                  $varAnhoComp = $anho;
                  $i++;

                }


                $botonesMes[$j]->anho = $anho;
                $botonesMes[$j]->mes = $mes;
                $j++;
                $imgArticulos = get_field('imagen_repo');
                $textoArticulo = get_field('texto_introduccion');
                $tituloArticulo = get_the_title();


                $restoArticulos .='<div class="col-12 col-sm-6 col-md-4 p-2 art anho'.  $anho  .' anho'.  $anho  .''.$mes.'" data-anho="'. $anho .'" data-mes="'. $anho .'-'.$mes.'">
                                <a href="'. get_permalink() .'" role="article" style="color: #fff">
                                <div class="articulo">
                                    <div  class="capaTexto">
                                        <div class="fechaEvento">'. $dia .'  '.$mes.'</div><div class="anhoFecha">'. $anho .'</div>
                                       <h2 class="entry-title" style="margin: 9% 0;">
                                        '. $tituloArticulo .'
                                       </h2>
                                        <div class="entry-content">
                                            '. $textoArticulo  .'
                                        </div>

                                    </div>
                                    <div class="fondo" style="background-image: url('.$imgArticulos['url'].');"></div></div>
                                </a>
                                </div> ';



             }



              $my_current_lang = apply_filters( 'wpml_current_language', NULL );
              $verTodoVariable = '';
              if ($my_current_lang == 'es') {
                $verTodoVariable = 'ver todos';
              } elseif ($my_current_lang == 'en') {
                $verTodoVariable = 'view all';
              }


              $pintarBotonesAnho = '<div class="btn-group" role="group" aria-label="Basic outlined example"><button type="button" class="btn btn-outline-secundario" id="vertodos">'.$verTodoVariable.'</button>';
              for($varAnho = 0; $varAnho < count($botonesAnho); $varAnho++) {
                      $pintarBotonesAnho .= '<button type="button" class="btn btn-outline-secundario" id="anho'.$botonesAnho[$varAnho].'">'.$botonesAnho[$varAnho].'</button>';
              }

              $pintarBotonesAnho .= '</div>';


              $pintarBotonesMes = '<div class="btn-group" role="group" aria-label="Basic outlined example">';


              foreach ($botonesMes as $botonMes) {

                if($mesComodin != $botonMes->mes) {
                  $pintarBotonesMes .= '<button type="button" class="btn btn-outline-secundario meses mesesanho'.$botonMes->anho.'" id="'.$botonMes->mes.'" data-anho="anho'.$botonMes->anho.'">'.$botonMes->mes.'</button>';
                  $mesComodin = $botonMes->mes;
                }


              }
          $pintarBotonesMes .= '</div>';


               /* echo '<pre>';
                print_r($botonesAnho);
                echo '</pre>';
                echo '<pre>';
                print_r($botonesMes);
                echo '</pre>'; */


              ?>
              <div class="container py-2">
                <div class="row">
                  <div class="texto1 px-md-0 col-md-10 col-md-10 col-lg-10 offset-md-1">
                    <p><?php echo $introRepoEventos; ?></p>
                  </div>
                </div>
              </div>
              <div class="container py-4">
              <div class="row">
                <div class="texto1 px-md-0 col-md-10 col-md-10 col-lg-10 offset-md-1">
                <div style="width: 100%; display: block" id="botonesAnho"> <?php  echo $pintarBotonesAnho;?></div>
                  <div style="width: 100%; display: block" id="botonesMes"> <?php
                        echo $pintarBotonesMes;
                    ?></div>

                </div>
              </div>
            </div>
              <div class="container">
                <div class="row">
                  <div class="d-none col-lg-1 d-lg-block"></div>
                  <div class="col-12 col-lg-11">
                    <div class="row">
                      <?php  echo $restoArticulos; ?>
                    </div>
                  </div>
                </div>
              </div>

@endsection
