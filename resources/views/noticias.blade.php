{{--
  Template Name: Noticias
--}}

@extends('layouts.app')

@section('content')


      @while(have_posts()) @php the_post() @endphp
        @include('partials.page-header')

        <div class="container">
          <div class="row justify-content-center">
            @include('partials.content-page')
            <div class="infinite-scroll-container">
              @query( $args_noticias )
              @posts
                <a href="{{ get_permalink() }}" role="article" class="art d-flex my-4 py-sm-4">
                  <div class="img zoom">
                    @thumbnail("large")
                  </div>
                  <div class="textos">
                    <h2 class="entry-title pb-1 mb-2">@title</h2>
                    <div class="entry-summary">
                      @excerpt
                      @isfield('leer_mas', true)
                      <p class="leermas text-principal"><span class="text-secundario">+</span> ver más</p>
                      @endfield
                    </div>
                  </div>
                </a href="" role="article">
              @endposts
            </div>
          </div>
        </div>
      @endwhile

  <nav class="nav-links">
    <div class="next-link">
      {!! next_posts_link( 'Older Entries', $query->max_num_pages ) !!}
    </div>
    <div class="previous-link">
      {!! previous_posts_link( 'Newer Entries' )!!}
    </div>
  </nav>
  <div class="container">
    <div class="row">
      @include('partials.loader')
    </div>
  </div>



@endsection
