{{--
  Template Name: Merlin Hub App
--}}

@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp
    @include('partials.page-header')
      <div class="container-fluid">

        <div class="row my-5">
          <div class="col-12 col-md-6 offset-md-1 mb-5">
            @php the_content() @endphp
          </div>
          <div class="col-12 col-md-3 offset-md-1">
            <div class="badges">
              <div class="icono">
                <img src="{{ get_template_directory_uri() }}/svg/mh-logo.svg" alt="">
              </div>
              <h3><?php
                $my_current_lang = apply_filters( 'wpml_current_language', NULL );
                if ($my_current_lang == 'es') {
                ?>Descarga la app <span>de Merlin Hub</span><?php
                } elseif ($my_current_lang == 'en') {
                ?> Download the  <span>Merlin Hub App</span> <?php
                }
                ?></h3>
              <div class="insignias">
                <div class="insignia">
                  <a href="@field('mhapp_applestore')" class=""><img src="{{ get_template_directory_uri() }}/svg/badge-apple-store.svg" alt="Badge Apple Store"></a>
                </div>
                <div class="insignia">
                  <a href="@field('mhapp_googleplay')"><img src="{{ get_template_directory_uri() }}/svg/badge-google-play.svg" alt="Badge Google Play"></a>
                </div>
              </div>
            </div>
          </div>
        </div>

        @fields('mhapp_bloque')
        <div class="row my-5">
          <div class="col-12 col-md-1 offset-md-1 icoImagen">
            <img src="{{ PageTemplateMerlinHubApp::mhappIcon() }}" alt="" width="100%">
          </div>
          <div class="col-12 col-md-5 pl-md-5">
            @sub('mhapp_texto')
          </div>
        </div>
        @endfields

    </div>
  @endwhile
@endsection

