@extends('layouts.app')

@section('content')
  @include('partials.page-header-agenda')
  <div class="container">
    <div class="row">
      <div class="d-none col-lg-1 d-lg-block"></div>
      <div class="col-12 col-lg-11">
        <div class="row">
          <?php
          $restoArticulos = '';
          $destacadosSuperiores = '';
          ?>
                <div class="col-12 col-lg-11">
                  @php
                    $fechaHoy = date("y-m-d");
                    $args = array(
                        'post_type'=>'evento',
                        'posts_per_archive_page' => -1,
                        'suppress_filters' => false,
                        'meta_key'			=> 'fecha_comienzo',
	                      'orderby'			=> 'meta_value',
	                      'order'				=> 'ASC',
	                      'meta_query'  => array(
                            array(
                                'key'     => 'fecha_comienzo',
                                'value'   => $fechaHoy,
                                'compare' => '>=',
                                'type'    => 'DATE'
                            ),
                           )
                       );
                    $loop = new WP_Query($args);
                  @endphp

                    @if (!have_posts())
                      <div class="alert alert-warning">
                        {{ __('Sorry, no results were found.', 'sage') }}
                      </div>
                      {!! get_search_form(false) !!}
                    @endif



                    <div class="row">

                    @while ($loop -> have_posts()) @php $loop ->the_post() @endphp

                      <?php



                      $output = '';
                      $diaEvento = '';
                      $horaEvento = '';
                      $mesEvento = '';
                      if( have_rows('fecha_evento', $loop->ID )):

                        // loop through the rows of data

                        while ( have_rows('fecha_evento', $loop->ID)) : the_row();

                          echo $loop->ID;
                          $varDia = get_sub_field('dia_evento');
                          $diaEvento = date("d", strtotime($varDia));
                          $diaEvento .= '<br />';
                          $mesEvento = date('F', strtotime($varDia));
                          $output .= '<div class="diaEvento">'. $diaEvento .'</div>';
                          $horaEvento .= '<div class="horaEvento">'. get_sub_field('horarioEvento') .'</div>';
                          // $output .= '<div class="mesEvento">'. $mesEvento .'</div>';
                          switch ($mesEvento) {

                            case 'January':
                              $my_current_lang = apply_filters( 'wpml_current_language', NULL );
                              if ($my_current_lang == 'es') {
                                $mesTraducido = "Enero";
                              } elseif ($my_current_lang == 'en') {
                                $mesTraducido = "January";
                              }
                              $output .= '<div class="mesEvento">'. $mesTraducido .'</div>';
                              break;
                            case 'February':
                              $my_current_lang = apply_filters( 'wpml_current_language', NULL );
                              if ($my_current_lang == 'es') {
                                $mesTraducido = "Febrero";
                              } elseif ($my_current_lang == 'en') {
                                $mesTraducido = "February";
                              }
                              $output .= '<div class="mesEvento">'. $mesTraducido .'</div>';
                              break;
                            case 'March':
                              $my_current_lang = apply_filters( 'wpml_current_language', NULL );
                              if ($my_current_lang == 'es') {
                                $mesTraducido = "Marzo";
                              } elseif ($my_current_lang == 'en') {
                                $mesTraducido = "March";
                              }
                              $output .= '<div class="mesEvento">'. $mesTraducido .'</div>';
                              break;
                            case 'April':
                              $my_current_lang = apply_filters( 'wpml_current_language', NULL );
                              if ($my_current_lang == 'es') {
                                $mesTraducido = "Abril";
                              } elseif ($my_current_lang == 'en') {
                                $mesTraducido = "April";
                              }
                              $output .= '<div class="mesEvento">'. $mesTraducido .'</div>';
                              break;
                            case 'May':
                              $my_current_lang = apply_filters( 'wpml_current_language', NULL );
                              if ($my_current_lang == 'es') {
                                $mesTraducido = "Mayo";
                              } elseif ($my_current_lang == 'en') {
                                $mesTraducido = "May";
                              }
                              $output .= '<div class="mesEvento">'. $mesTraducido .'</div>';
                              break;
                            case 'June':
                              $my_current_lang = apply_filters( 'wpml_current_language', NULL );
                              if ($my_current_lang == 'es') {
                                $mesTraducido = "Junio";
                              } elseif ($my_current_lang == 'en') {
                                $mesTraducido = "June";
                              }
                              $output .= '<div class="mesEvento">'. $mesTraducido .'</div>';
                              break;
                            case 'July':
                              $my_current_lang = apply_filters( 'wpml_current_language', NULL );
                              if ($my_current_lang == 'es') {
                                $mesTraducido = "Julio";
                              } elseif ($my_current_lang == 'en') {
                                $mesTraducido = "July";
                              }
                              $output .= '<div class="mesEvento">'. $mesTraducido .'</div>';
                              break;
                            case 'Agost':
                              $my_current_lang = apply_filters( 'wpml_current_language', NULL );
                              if ($my_current_lang == 'es') {
                                $mesTraducido = "Agosto";
                              } elseif ($my_current_lang == 'en') {
                                $mesTraducido = "Agost";
                              }
                              $output .= '<div class="mesEvento">'. $mesTraducido .'</div>';
                              break;
                            case 'September':
                              $my_current_lang = apply_filters( 'wpml_current_language', NULL );
                              if ($my_current_lang == 'es') {
                                $mesTraducido = "Septiembre";
                              } elseif ($my_current_lang == 'en') {
                                $mesTraducido = "September";
                              }
                              $output .= '<div class="mesEvento">'. $mesTraducido .'</div>';
                              break;
                            case 'October':
                              $my_current_lang = apply_filters( 'wpml_current_language', NULL );
                              if ($my_current_lang == 'es') {
                                $mesTraducido = "Octubre";
                              } elseif ($my_current_lang == 'en') {
                                $mesTraducido = "October";
                              }
                              $output .= '<div class="mesEvento">'. $mesTraducido .'</div>';
                              break;
                            case 'November':
                              $my_current_lang = apply_filters( 'wpml_current_language', NULL );
                              if ($my_current_lang == 'es') {
                                $mesTraducido = "Noviembre";
                              } elseif ($my_current_lang == 'en') {
                                $mesTraducido = "November";
                              }
                              $output .= '<div class="mesEvento">'. $mesTraducido .'</div>';
                              break;
                            case 'December':
                              $my_current_lang = apply_filters( 'wpml_current_language', NULL );
                              if ($my_current_lang == 'es') {
                                $mesTraducido = "Diciembre";
                              } elseif ($my_current_lang == 'en') {
                                $mesTraducido = "December";
                              }
                              $output .= '<div class="mesEvento">'. $mesTraducido .'</div>';
                              break;
                          }

                          $diaEvento = '';
                          $output .= $horaEvento;
                          $horaEvento = '';


                        endwhile;
                        $output .= '<div class="diaEvento">'. $diaEvento .'</div>';
                        $diaEvento = '';
                        $output .= $horaEvento;
                        $horaEvento = '';

                      else :

                        // no rows found

                      endif;


                      //echo $post->ID;
                      $lugares = get_field('lugar_evento', $loop->ID);
                      // print_r($lugares);
                      $output2 = '';
                      // print_r($lugares);
                      if( $lugares ):
                        $output2 .= '<div href="'. $lugares[0]->guid .'" class="linkLugarEvento">'. $lugares[0]->post_title .'</div>';
                        // $output2 .=  $lugares[0]->post_title;
                      endif;


                      $esDestacado = get_field('destacado');
                      $imagenNueva = get_field('imagenDestacado');
                      $imgArticulos = get_field('imagen_repo');

                      $textoArticulo = get_field('texto_introduccion');
                      $tituloArticulo = get_the_title();

                      if($esDestacado == 1) {
                        $destacadosSuperiores .= '<div class="col-12 destacado">
                                  <a href="'. get_permalink() .'" role="article" style="color: #fff">
                                     <div class="articulo">
                                              <div  class="capaTexto">'. $output .'
                                              <h2 class="entry-title" style="margin: 2% 0;">'. $tituloArticulo .'</h2>
                                              <div class="entry-content">'. $textoArticulo  .'
                                                </div>'. $output2 .'
                                     </div>
                                     <div class="fondo" style="background-image: url('. $imagenNueva['url'].');"></div>
                                   </div>
                                  </a>
                                </div>';
                      }  else {
                        $restoArticulos .='<div class="col-12 col-sm-6 col-md-4 p-2 art">
                                <a href="'. get_permalink() .'" role="article" style="color: #fff">
                                <div class="articulo">
                                    <div  class="capaTexto">'. $output .'
                                       <h2 class="entry-title" style="margin: 9% 0;">
                                        '. $tituloArticulo .'
                                       </h2>
                                        <div class="entry-content">
                                            '. $textoArticulo  .'
                                        </div>
                                        '. $output2 .'
                                    </div>
                                    <div class="fondo" style="background-image: url('.$imgArticulos['url'].');"></div></div>
                                </a>
                                </div> ';
                      }


                      ?>


                      @include('partials.content-'.get_post_type())

                    @endwhile
<?php  echo  $destacadosSuperiores;

                      echo $restoArticulos;?>
                      </div>

                    </div>
                  </div>
          </div>
        </div>
      </div>
    </div>
  @endsection
