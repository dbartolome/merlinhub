{{--
  Template Name: El norte es el centro
--}}

@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp
    @include('partials.page-header')
  @endwhile
  <section class="textoSup">
    <div class="container-fluid">
      <div class="row">
        <div class="texto1 px-md-0 col-md-10 col-md-8 col-lg-7 offset-md-1">
          @field('eneec_txt1')
        </div>
      </div>
    </div>
  </section>
  <section class="mapSup">
    <div class="container-fluid">
    <div class="row">
      <div class="col-md-11 px-md-0 offset-md-1">
        {!! $eneec_mapa1 !!}
      </div>
    </div>
    </div>
  </section>

      <section class="textoSup2">
        <div class="container-fluid">
  <div class="row">
    <div class="texto1 col-md-10 px-md-0 col-md-8 col-lg-7 offset-md-1">
      @field('eneec_txt1b')
    </div>
  </div>
  </div>
  </section>
  <section class="">
    <div class="container-fluid">
    <div class="row my-5">
      <div class="col-md-6 col-lg-5 px-md-0 offset-md-1 align-self-center">
      <div class="texto2">
        @field('eneec_txt2')
      </div>
      </div>
   <!-- <div class="piedemapa">
      @field('eneec_piedemapa')
    </div> -->
      <div class="col-md-6 col-lg-6 align-self-center">
        <div class="mapa2">
          {!! $eneec_mapa2 !!}
        </div>
    </div>
    </div>
  </div>
  </section>
  <section>
    <div class="container-fluid">
    <div class="row my-5">
      <div class="col-sm-6 mb-3 pl-0 align-self-center">
        {!! $eneec_img1 !!}
      </div>
      <div class="col-md-6 col-lg-4 align-self-center">
        @field('eneec_txt3')
      </div>
    </div>
  </div>
  </section>
<section>
  <div class="container-fluid">
    <div class="row my-5">
      <div class="col-md-5 px-md-0 offset-md-1 align-self-center">
        @field('eneec_txt4')
      </div>
      <div class="col-sm-6 col-md-4 col-lg-4 align-self-center">
        {!! $eneec_img2 !!}
      </div>
    </div>
  </div>
</section>
  <!--
  <div class="container">
    <div class="row my-5">
      <div class="col-sm-5 offset-sm-1">
        {!! $eneec_img3 !!}
      </div>
      <div class="col-sm-5">
        @field('eneec_txt5')
      </div>
    </div>
  </div>
-->
@endsection
