

@extends('layouts.app')
@section('content')
  @while(have_posts()) @php the_post() @endphp
 @include('partials.content-single-'.get_post_type())
  @endwhile
@endsection

<script type='text/javascript' src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js'></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-mousewheel/3.1.13/jquery.mousewheel.js"></script>

<script>

  $(document).ready(function() {
    var ventana_ancho = $(window).width();
    if(ventana_ancho > 768) {
      $('html, body, *').on('mousewheel', function (e, delta) {
        this.scrollLeft -= (delta * 40);
      });
    }
  });
</script>


