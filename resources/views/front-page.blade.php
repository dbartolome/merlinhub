@extends('layouts.app')
@section('content')
  <div class="container-fluid noMargin" class="noMargin" style="margin: 0; padding: 0;">
    <div class="row noMargin" style="margin: 0; padding: 0;">
      <div class="col-12 " style="margin: 0; padding: 0;">
        <?php
        $nomSlider = get_field('NombreSlider','option');
        add_revslider($nomSlider);
        ?>

      </div>
    </div>
  </div>
  <!-- Contendor destacado princiapl -->
  <div class="container destacadoPrincipal">
    <div class="row fondoNoticia" style="background-image: url({!! $imagen_fondo !!});">
        <div class="col-12 col-lg-6 capaIzquierda">
          <div class="capaTexto">
          <h2 class="tituloNoticia" style="color: #fff">{!! $titulo_destacado !!}</h2>
            <p>{!! $texto_destacado !!}</p>
            {!! $enlace_destacado !!}
          </div>
        </div>

    </div>
  </div>
  <!-- Contendor Programa App -->
  <div class="container contPrograma">
    {!! $programa_app !!}


  </div>
  <!-- Contendor Separador -->
  <hr class="separador">
  <!-- Contendor Territorios -->
  <section class="container contTerritorios">
  {!! $crear_territorio !!}
  </section>
  <!-- Contendor Explora mapa o figura -->
  <section class="contExplora">
        <h2 class="titSeccion">{!! $titulo_seccion_explora !!}</h2>
        <hr class="lineacontinua">
        <div class="container">
            <div class="row sinPadding">
              <div class="col-12 col-lg-11 sinPadding">

                {!! $codigo_seccion_explora !!}

              </div>
            </div>
            <div class="row sinPadding">
              <div class="col-12 col-lg-11 my-4" style="text-align: end">
                {!! $boton_ver_espacios !!}
              </div>
            </div>
        </div>
  </section>
  <!-- Contendor Noticia -->
  <section class="contNoticia">
        <h2 class="titSeccion">{!! $titulo_seccion_noticia !!}</h2>
        <hr class="lineacontinua">
        <div class="container">
        <?php
         $varLink = get_field('enlace_noticia', 'option');
         ?>
        <a href="<?php echo $varLink ?>">

        <div class="row align-items-center sinPadding">
          <div class="col-12 mx-0 mr-0 col-lg-4 mx-lg-3 mr-lg-5 sinPadding">
            <div class="textoNoticia">
              <h2>{!! $titulo_noticia !!}</h2>
              <hr class="lineaDerecha">
              <p>{!! $txt_noticia !!}</p>
              {!! $enlace_noticia !!}
            </div>
          </div>
          <div class="col-12 col-lg-8 col-xl-6">{!! $imagen_noticia !!}</div>
        </div>
        </a>
      </div>
  </section>
  <!-- Contendor Noticia
  <div class="container contNoticia">
    <div class="row sinPadding">
      <div class="col-12 offset-0 col-lg-10 offset-lg-2 capaIzquierda sinPadding">
        <h2 class="titSeccion">{!! $titulo_seccion_noticia !!}</h2>
        <hr class="lineacontinua">
        <?php
         $varLink = get_field('enlace_noticia', 'option');
         ?>
        <a href="<?php echo $varLink ?>">
        <div class="row align-items-center sinPadding">
          <div class="col-12 col-lg-4 sinPadding mx-3 mr-5">
            <div class="textoNoticia">
              <h2>{!! $titulo_noticia !!}</h2>
              <hr class="lineaDerecha">
              <p>{!! $txt_noticia !!}</p>
              {!! $enlace_noticia !!}
            </div>
          </div>
          <div class="col-12 col-lg-6">{!! $imagen_noticia !!}</div>
        </div>
        </a>
      </div>
    </div>
  </div>  -->
  <!-- Contendor AGENDA -->
  <section class="contAgenda">
        <h2 class="titSeccion">
          {!! $titulo_seccion_agenda !!}
        </h2>
        <hr class="lineacontinua">
    <div class="container">
        <div class="row sinPadding">
          <div class="col-12 col-lg-11">
            <div class="row">
              @include('partials.front-page.gridEventos')
            </div>
          </div>
        </div>
        <div class="row sinPadding">
          <div class="col-12 col-lg-11 mt-4">
            <div class="row alinearDer">
              <div class="col-12" style="text-align: end;">
              {!! $boton_ver_eventos !!}
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <!-- Contendor Comunidad -->
  <section class="contComunidad">
        <h2 class="titSeccion">
          {!! $titulo_seccion_comunidad !!}
        </h2>
        <hr class="lineacontinua">
        <div class="container">
        <div class="row sinPadding">
          <div class="col-12 col-lg-10">
            <div class="row">
          {!! $ver_logos !!}
          </div>
          </div>
        </div>

      </div>
    </div>
  </section>

@endsection
