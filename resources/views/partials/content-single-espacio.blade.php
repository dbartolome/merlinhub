
<div class="contenedor d-flex flex-wrap">

  <div class="col-izq">

    <header class="d-flex flex-wrap mb-5 mb-lg-2 p-4">
      <h1 class="entry-title text-principal">{!! get_the_title() !!}</h1>
      <nav class="nav-links">
        {{ previous_post_link('%link', '<span>&laquo;</span> %title') }} {{ next_post_link('%link', '%title <span>&raquo;</span>') }}
      </nav>


    </header>

    <section class="entry-content p-4">
      <p> {!! $direccion_espacio !!}</p>
      <p>{!! $ciudad_espacio !!}</p>
      @php the_content() @endphp
    </section>

    <section class="p-4 iconos d-flex flex-wrap align-items-start justify-content-center">
      <?php
      $my_current_lang = apply_filters( 'wpml_current_language', NULL );
      $varEdificios = '';
      $varPersonas = '';
      $varEmpresas = '';
      if ($my_current_lang == 'es') {

        $varEdificios = 'Edificios';
        $varPersonas = 'Perosonas';
        $varEmpresas = 'Empresas';
      } elseif ($my_current_lang == 'en') {
        $varEdificios = 'Buildings';
        $varPersonas = 'People';
        $varEmpresas = 'Companies';
      }
      ?>
      @hasfield('espacios_superficie')
        <article>
          @svg('icono-superficie')
          <ul>
            <li>
              @field('espacios_superficie') m<sup>2</sup>
            </li>
            @hasfield('espacios_edificios')
              <li>
                @field('espacios_edificios') <?php echo $varEdificios; ?>
              </li>
            @endfield
          </ul>
        </article>
      @endfield

      @hasfield('espacios_personas')
        <article>
          @svg('icono-personas')
          <ul>
            <li>
              @field('espacios_personas') <?php echo $varPersonas; ?>
            </li>
          </ul>
        </article>
      @endfield

      @hasfield('espacios_empresas')
        <article>
          @svg('icono-empresas')
          <ul>
            <li>
              @field('espacios_empresas') <?php echo $varEmpresas; ?>
            </li>
          </ul>
        </article>
      @endfield

      @if (get_field('espacios_loom'))
        <article>
          @svg('icono-loom')
          @if (get_field('espacios_flexspaces'))
          <ul>
            <li>
              Flexspaces by loom
            </li>
          </ul>
          @endif
        </article>
      @endif

      @hasfield('espacios_espacios_comunes')
        <article>
          @svg('icono-comunes')
          <?php
          echo '<ul>';
          $terms = get_field('espacios_espacios_comunes');
          foreach( $terms as $term ) {
            echo '<li>'.$term->name.'</li>';
          }
          echo '</ul>';
          ?>
        </article>
      @endfield

      @hasfield('espacios_wellness')
        <article>
          @svg('icono-wellness')
          <?php
          echo '<ul>';
          $terms = get_field('espacios_wellness');
          foreach( $terms as $term ) {
            echo '<li>'.$term->name.'</li>';
          }
          echo '</ul>';
          ?>
        </article>
      @endfield

      @hasfield('espacios_restaurantes')
        <article>
          @svg('icono-restaurantes')
          <?php
          echo '<ul>';
          $terms = get_field('espacios_restaurantes');
          foreach( $terms as $term ) {
            echo '<li>'.$term->name.'</li>';
          }
          echo '</ul>';


          ?>
        </article>
      @endfield

      @hasfield('espacios_servicios')
        <article>
          @svg('icono-servicios')
          <?php
          echo '<ul>';
          $terms = get_field('espacios_servicios');
          foreach( $terms as $term ) {
             echo '<li>'.$term->name.'</li>';
          }
          echo '</ul>';


          ?>
        </article>
      @endfield

      @hasfield('espacios_comunidad')
        <article>
          @svg('icono-comunidad')
        <?php
          echo '<ul>';
          $terms = get_field('espacios_comunidad');
          foreach( $terms as $term ) {
            echo '<li>'.$term->name.'</li>';
          }
          echo '</ul>';
          ?>
        </article>
      @endfield

    </section>
<?php
    $my_current_lang = apply_filters( 'wpml_current_language', NULL );
    $verMicrositeVariable = '';
    $verTourVariable = '';
    $verCertificadosVariable = '';
    if ($my_current_lang == 'es') {

      $verMicrositeVariable = 'Ver microsite';
      $verTourVariable = 'Ver tour';
      $verCertificadosVariable = 'Certificaciones de calidad';
    } elseif ($my_current_lang == 'en') {
      $verMicrositeVariable = 'View microsite';
      $verTourVariable = 'View tour';
      $verCertificadosVariable = 'Quality certifications';
    }
    ?>
    @if (get_field('espacios_microsite_url') || get_field('espacios_tour') )
      <section class="microsite p-4 d-flex justify-content-center mb-3">
        @hasfield('espacios_microsite_url')
          <a href="@field('espacios_microsite_url')" target="_blank" class="px-3 py-2"><?php echo $verMicrositeVariable; ?></a>
        @endfield
        @hasfield('espacios_tour')
          <a href="@field('espacios_tour')" target="_blank" class="mx-2 px-3 py-2"><?php echo $verTourVariable; ?></a>
        @endfield
      </section>
    @endif

    @hasfield('espacios_certificados')

      <section class="calidad p-4 d-flex flex-wrap justify-content-center">
        <div style="text-align: center; display: block; width: 100%; margin-bottom: 10px"> <?php echo $verCertificadosVariable; ?> </div>
        @foreach ($espacios_cert as $cert)
            <div class="cert p-4">
              <img src="@asset($cert)">
            </div>
        @endforeach
      </section>
    @endfield
<!--
    <?php
    $id = get_the_ID();

    $args = array(
      'post_type'=>'empresa',
      'posts_per_archive_page' => -1,
      'meta_query' => array(
   array(
      'key' => 'espacioEmpresa',
      'value' => '"' . $id . '"',
      'compare' => 'LIKE'
      )
   )
      );
    $loop = new WP_Query($args);
    ?>




    <section class="empresas p-4 mb-4">
      <div style="text-align: center; font-size: 1rem;">Directorio de empresas</div>
      <div class="container-fluid">
        <div class="row">
          @while($loop->have_posts()) @php $loop->the_post() @endphp
          <div class="col-6 col-md-3 p-3 text-center">
            <?php
            $enlaceEmpresa = get_field('enlaceEmpresa');
            $urlImagen = get_the_post_thumbnail_url();
            $nombreEmpresa = get_the_title();
            if ($enlaceEmpresa != '') {
              echo '<a href="'.$enlaceEmpresa.'" target="_blank">';
              echo '<img src="'.$urlImagen.'" class="imgLogoEmpresa">';
              echo '<div class="etiquetaEmpresa" >'. $nombreEmpresa .'</div>';
              echo '</a>';
            } else {

              echo '<img src="'.$urlImagen.'" class="imgLogoEmpresa">';
              echo '<div class="etiquetaEmpresa">'. $nombreEmpresa .'</div>';

            }



            ?>

          </div>

          @endwhile


        </div>
      </div>
    </section>
    <style>
      .etiquetaEmpresa {
        text-transform: uppercase;
        font-size: 0.7rem;
        color:#282270;
        padding: 1% 0;
        font-weight: bold;
      }

      .imgLogoEmpresa {
        width: 50%;
        filter: grayscale(100%);
      }
    </style>

    <?php
      wp_reset_query();
    ?>
-->
    <section class="mapa">

      <?php
      $codigoMapa = get_field('mapa_espacio');
      echo do_shortcode($codigoMapa);
      ?>
    </section>
  </div>


  <div class="galeria fija col-der d-flex align-items-end">
    @hasfield('espacios_imagenes_galeria')
        <div class="glide">

          <div class="glide__track" data-glide-el="track">
            <ul class="glide__slides">
              @fields('espacios_imagenes_galeria')
                @php $id = get_sub_field('espacios_imagen_galeria')['id'] @endphp
                @component('components.bg-image-slider', ['image' => $id]) @endcomponent
              @endfields
            </ul>
          </div>

          <div class="glide__arrows" data-glide-el="controls">
            <button class="glide__arrow glide__arrow--left" data-glide-dir="<">
              @svg('flecha-slider-izq')
            </button>
            <button class="glide__arrow glide__arrow--right" data-glide-dir=">">
              @svg('flecha-slider-der')
            </button>
          </div>

          <div class="glide__bullets" data-glide-el="controls[nav]">
            @for ($i = 0; $i < $glide_bullets; $i++)
          <button class="glide__bullet" data-glide-dir="={{ $i }}"></button>
            @endfor
          </div>

        </div>
    @endfield
  </div>

</div>

