<div class="page-header-agenda">
<?php
  $image = get_field('img_header', 'option');
  $tituloSeccRepo = get_field('titSeccionEventosRepo', 'option');
  $size = "100%";
  if( $image ) {
    $output = '<img src="'.$image['url'].'" alt="'. $image['title'].'" width="'. $size .'">';
  }

  echo $output;
?>
  <div class="container pt-5 pb-2">
    <div class="row">
      <div class="d-none col-lg-1 d-lg-block"></div>
      <div class="col-12 col-lg-11 page-header">
        <div class="row">
          <div class="col-12">
          <h1 class="text-principal mt-3"><?php echo $tituloSeccRepo; ?></h1>
        </div>
        </div>
      </div>
    </div>
  </div>
</div>
