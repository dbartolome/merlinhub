<header class="banner">
  <div class="contMenuPeq">
    <a class="brand" id="brandPrincipal" href="{{ home_url('/') }}"><img src="@option('logo_peq','url')" alt="{{ get_bloginfo('name', 'display') }}"></a>
    <button class="hamburger hamburger--spin hgrande" type="button">
          <span class="hamburger-box">
            <span class="hamburger-inner"></span>
          </span>
    </button>
    <div class="banderasHeader">@php dynamic_sidebar('lenguaje-header') @endphp</div>

    <div class="redesHeader" >
      <ul class="listaRedes">
        @fields('social','option')
        <li class="itemRedes"><a href="@sub('social_url')" target="_blank"><img src="@sub('social_icon', 'url')" alt="@sub('social_icon', 'alt')"/></a></li>
        @endfields
      </ul>
    </div>
  </div>
  <div class="container" id="contMenu">
    <div class="row">
      <div class="col-12 colUno">
        <a class="brand" href="{{ home_url('/') }}"><img src="@option('logo_gra','url')" alt="{{ get_bloginfo('name', 'display') }}"></a>
      </div>
      <div class="col-12 align-self-center">
        <nav class="nav-primary">
          @if (has_nav_menu('primary_navigation'))
            {!! wp_nav_menu($primarymenu) !!}
          @endif

        </nav>

      </div>
      <div class="col-12 colTres">

      </div>
    </div>
  </div>
</header>
<div class="headerMovil">
  <a class="brand" href="{{ home_url('/') }}">
    <img src="@option('logo_gra','url')" alt="{{ get_bloginfo('name', 'display') }}" style="max-height: 50px;">
  </a>
  <div class="banderasMovil"> @php dynamic_sidebar('lenguaje-menu') @endphp </div>
  <button class="hamburger hamburger--spin" type="button" style="float:right">
          <span class="hamburger-box">
            <span class="hamburger-inner"></span>
          </span>
  </button>
</div>
