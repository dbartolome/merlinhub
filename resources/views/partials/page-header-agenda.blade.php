<div class="page-header-agenda">
<?php
  $image = get_field('img_header', 'option');
  $size = "100%";
  if( $image ) {
    $output = '<img src="'.$image['url'].'" alt="'. $image['title'].'" width="'. $size .'">';
  }

  echo $output;
?>
  <div class="container">
    <div class="row">
      <div class="d-none col-lg-1 d-lg-block"></div>
      <div class="col-12 col-lg-11">
        <div class="row">
          <div class="col-12">
          <h1 class="text-principal mt-3"><?php
           $my_current_lang = apply_filters( 'wpml_current_language', NULL );
            if ($my_current_lang == 'es') {
            ?>Eventos<?php
            } elseif ($my_current_lang == 'en') {
            ?> Events <?php
            }
            ?></h1>
        </div>
        </div>
      </div>
    </div>
  </div>
</div>
