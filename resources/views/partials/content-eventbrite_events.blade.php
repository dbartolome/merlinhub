<?php
  $imagenDestacada = get_the_post_thumbnail_url();


global $iee_events;

if ( ! isset( $event_id ) || empty( $event_id ) ) {
  $event_id = get_the_ID();
}

$start_date_str      = get_post_meta( $event_id, 'start_ts', true );
$end_date_str        = get_post_meta( $event_id, 'end_ts', true );
$mes_start_date_formated = date_i18n( 'F', $start_date_str );
$dia_start_date_formated = date_i18n( 'j', $start_date_str );
$end_date_formated   = date_i18n( 'F j', $end_date_str );
$website             = get_post_meta( $event_id, 'iee_event_link', true );

$iee_options = get_option( IEE_OPTIONS );
$time_format = isset( $iee_options['time_format'] ) ? $iee_options['time_format'] : '12hours';
if($time_format == '12hours' ){
  $start_time          = date_i18n( 'h:i a', $start_date_str );
  $end_time            = date_i18n( 'h:i a', $end_date_str );
}elseif($time_format == '24hours' ){
  $start_time          = date_i18n( 'G:i', $start_date_str );
  $end_time            = date_i18n( 'G:i', $end_date_str );
}else{
  $start_time          = date_i18n( get_option( 'time_format' ), $start_date_str );
  $end_time            = date_i18n( get_option( 'time_format' ), $end_date_str );
}

$elContenidoExtUnp = get_the_content();
$elContenidoExt = strip_tags($elContenidoExtUnp);
$str = get_the_title();
$contadorNum = strlen($str);
$elContenido = substr($elContenidoExt, $contadorNum, 250);

$venue_name       = get_post_meta( $event_id, 'venue_name', true );
?>
<div class="col-12 col-md-6 col-lg-4 p-2">
      <a href="<?php the_permalink(); ?>" style="background-image: url(<?php echo $imagenDestacada; ?>);" class="fondoLink">
        <div class="row p-0 m-0 altoCompleto">
          <div class="col-7 p-3 capaTexto">
            <div class="fechaIni">
              <?php echo $dia_start_date_formated; ?>
            </div>
            <div class="fechaFin">
              <?php echo $mes_start_date_formated; ?>
            </div>
            <div class="fechasCapa"><?php echo $start_time; ?> - <?php echo $end_time; ?></div>
            <div class="capaTitulo"> {{  the_title()}}</div>
            <div style="color: #fff; font-size: 0.8rem; text-align: left; font-weight: lighter; line-height: 1.2"><?php echo $elContenido; ?></div>

            <div class="linkLugarEvento"><?php echo $venue_name; ?></div>
         </div>
          <div class="col-5"></div>
        <!-- <img src="<?php echo $imagenDestacada; ?>" width="100%"> -->
       </div>
      </a>
</div>
