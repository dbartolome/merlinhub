@include("partials.page-header-single-post")
<?php
$post_date = get_the_date( 'F Y' );
?>
<div class="container">
  <div class="row">
    <div class="col-md-9 offset-md-1 px-md-0">
      <div style="text-align: right; font-weight: bold; text-transform: capitalize; color: #13db00; margin-bottom: 3rem;"><?php  echo $post_date; ?></div>
      <article @php post_class() @endphp>
        <div class="entry-content">
          @php the_content() @endphp
        </div>
      </article>
    </div>
  </div>
</div>
