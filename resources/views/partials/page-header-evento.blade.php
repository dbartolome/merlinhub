<div class="page-header">

  <img src="<?php echo get_the_post_thumbnail_url(get_the_ID(),'full'); ?>" alt="My Image" width="100%"/>

  <div class="container">
    <div class="row">
      <div class="d-none col-lg-1 d-lg-block"></div>
      <div class="col-12 col-lg-11">
        <div class="row">
          <div class="col-12">
            <h1 class="text-principal mt-3">{{the_title()}}</h1>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
