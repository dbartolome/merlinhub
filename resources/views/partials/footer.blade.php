<footer id="footer" class="content-info">
  <div class="container">
    <div class="row">
      <div class="offset-md-1 col-12 col-md-2 col-lg-2 col-xl-2 logoCentrado">
        <?php
        $image = get_field('logo_footer', 'option');
        $size = "100%";
        if( $image ) {
          $output = '<img src="'.$image['url'].'" class="'. $image['title'].'" alt="'. $image['title'].'" width="'. $size .'">';
        }

        echo $output;
        ?>
      </div>
      <div class="col-12 col-md-9 col-lg-9 col-xl-9 menuFooterCentrado">

        @php dynamic_sidebar('sidebar-footer') @endphp
      </div>
    </div>
    <div class="row">
      <div class="d-none col-lg-1 d-lg-block"></div>
      <div class="col-9 col-lg-11">
        @php dynamic_sidebar('bottom-footer') @endphp
      </div>
    </div>
  <div class="row justify-content-center">
    <div class="col-12 col-md-6">
      @php dynamic_sidebar('newsletter-footer') @endphp
    </div>
  </div>

  <div class="row footerLegales">
    <div class="col-12">
      @php dynamic_sidebar('legales-footer') @endphp
    </div>
  </div>
  </div>
</footer>


