<?php
global $iee_events;
$imagenDestacada = get_the_post_thumbnail_url();

if ( ! isset( $event_id ) || empty( $event_id ) ) {
$event_id = get_the_ID();
}

$start_date_str      = get_post_meta( $event_id, 'start_ts', true );
$end_date_str        = get_post_meta( $event_id, 'end_ts', true );
$mes_start_date_formated = date_i18n( 'F', $start_date_str );
$dia_start_date_formated = date_i18n( 'j', $start_date_str );
$website             = get_post_meta( $event_id, 'iee_event_link', true );

$iee_options = get_option( IEE_OPTIONS );
$time_format = isset( $iee_options['time_format'] ) ? $iee_options['time_format'] : '12hours';
if($time_format == '12hours' ){
  $start_time          = date_i18n( 'h:i a', $start_date_str );
  $end_time            = date_i18n( 'h:i a', $end_date_str );
}elseif($time_format == '24hours' ){
  $start_time          = date_i18n( 'G:i', $start_date_str );
  $end_time            = date_i18n( 'G:i', $end_date_str );
}else{
  $start_time          = date_i18n( get_option( 'time_format' ), $start_date_str );
  $end_time            = date_i18n( get_option( 'time_format' ), $end_date_str );
}
$venue_name       = get_post_meta( $event_id, 'venue_name', true );
$venue_address    = get_post_meta( $event_id, 'venue_address', true );
$venue['city']    = get_post_meta( $event_id, 'venue_city', true );
$venue['state']   = get_post_meta( $event_id, 'venue_state', true );
$venue['country'] = get_post_meta( $event_id, 'venue_country', true );
$venue['zipcode'] = get_post_meta( $event_id, 'venue_zipcode', true );
$venue['lat']     = get_post_meta( $event_id, 'venue_lat', true );
$venue['lon']     = get_post_meta( $event_id, 'venue_lon', true );
$venue_url        = esc_url( get_post_meta( $event_id, 'venue_url', true ) );


?>


  <div class="container">
    <div class="row">
      <div class="col-11 col-xs-3 col-md-3 col-lg-3 col-xxl-3">
            <?php
            if ( date( 'Y-m-d', $start_date_str ) == date( 'Y-m-d', $end_date_str ) ) {
            ?>

              <div style="text-transform: uppercase; font-size: 9.7rem; line-height: 1; font-weight: lighter; text-align: center">
                <?php echo $dia_start_date_formated; ?>
              </div>
              <div style="text-transform: uppercase; font-size: 3rem; line-height: 1; text-align: center">
                <?php echo $mes_start_date_formated; ?>
              </div>
              <div style="text-transform: uppercase; font-size: 0.8rem; line-height: 1; text-align: center">de <?php echo $start_time . ' a ' . $end_time; ?></div>
            <?php
            } else {
            ?>
      </div>
    <?php
              } ?>
      </div>
      <div class="col-11 col-xs-9 col-md-9 col-lg-8 col-xxl-7">
            <div class="row mb-3">
              <div class="col-4">
                <strong>Ubicación</strong>
                <div class="linkLugarEvento"><?php echo $venue_name; ?></div>
              </div>
              <?php
              // Organizer
              $org_name  = get_post_meta( $event_id, 'organizer_name', true );
              if ( $org_name != '' ) {
              ?>

        <div class="col-4">
          <strong>Organiza</strong>
          <div style=" font-size: 1.3rem;font-wight: bold; text-transform: uppercase;"><?php echo $org_name; ?></div>
        </div>
        <?php } ?>

             <?php
              if ( $website != '' ) { ?>
              <div class="col-4">
                <a href="<?php echo esc_url( $website ); ?>" style="height: 100%; display: block; font-size: 1.3rem; background-color:#282270; padding: 5% 0; border-radius: 10px; color: #fff; text-transform: uppercase; text-align: center">INSCRIBIRSE </a>
              </div>
              <?php } ?>


            </div>

            <div class="row">
              <div class="col-12 contenido">

              @php the_content() @endphp
              </div>
            </div>
          </div>
        </div>
  </div>
</div>

<div class="container">


  <?php

  if ( $venue_name != '' && ( $venue_address != '' || $venue['city'] != '' ) ) {
  ?>
  <div class="iee_organizermain library">
    <div class="venue">
      <div class="titlemain"> <?php esc_html_e( 'Venue', 'import-eventbrite-events' ); ?> </div>
      <p><?php echo $venue_name; ?></p>
      <?php
      if ( $venue_address != '' ) {
        echo '<p><i>' . $venue_address . '</i></p>';
      }
      $venue_array = array();
      foreach ( $venue as $key => $value ) {
        if ( in_array( $key, array( 'city', 'state', 'country', 'zipcode' ) ) ) {
          if ( $value != '' ) {
            $venue_array[] = $value;
          }
        }
      }
      echo '<p><i>' . implode( ', ', $venue_array ) . '</i></p>';
      ?>
    </div>
    <?php
    if ( $venue['lat'] != '' && $venue['lon'] ) {
    ?>
    <div class="map">
      <iframe src="https://maps.google.com/maps?q=<?php echo $venue['lat'] . ',' . $venue['lon']; ?>&hl=es;z=14&output=embed" width="100%" height="350" frameborder="0" style="border:0; margin:0;" allowfullscreen></iframe>
    </div>
    <?php
    }
    ?>
    <div style="clear: both;"></div>
  </div>
  <?php
  }
  ?>
</div>


<style>
  .linkLugarEvento {
    color: #282270;
    text-transform: uppercase;
    font-weight: 700;
    font-size: 1.3rem;
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    line-height: 1.2;
  }
  .linkLugarEvento:before {
    content: url(/wp-content/themes/merlinhub/dist/images/locLugarEvento.png);
    margin-right: 10px;
    vertical-align: middle;
  }
  .contenido img {
    width: 100%;
  }
</style>

