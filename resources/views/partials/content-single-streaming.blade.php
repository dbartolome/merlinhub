<?php

  $fechaStreaming = get_field('fechaStreaming');
  $lugarStreaming = get_field('lugarStreaming');
  $scriptIframe = get_field('scriptIframe');
  $tituloSeccion = get_field('tituloSeccion');
  $contPonentes = '';
  $imagenStreaming = get_field('imagenStreaming');

  if( have_rows('ponentes') ){
    while( have_rows('ponentes') ) {
      the_row();
      $fotoPonente = get_sub_field('fotoPonente');
      $nombrePonente = get_sub_field('nombrePonente');
      $puestoPonente = get_sub_field('puestoPonente');
      $moderadorPonente = get_sub_field('moderadorPonente');


        $contPonentes .= '<div class="col-6 col-md-4 p-2 mb-3 text-center"><div class="moderaLabel">'.$moderadorPonente.'</div><img src="'. $fotoPonente["url"] .'" class="fotoPonente"><div class="nombrePonente">'.$nombrePonente.'</div><div class="puestoPonente">'.$puestoPonente.'</div></div>';


    }
  }





?>
  <div class="container">
    <div class="row justify-content-center">

      <div class="col-12 col-md-8">
        <div class="fechaStreaming"><?php echo $fechaStreaming; ?></div>

        <div class="lugarStreaming"> <?php echo $lugarStreaming; ?></div>
      </div>
      <div class="col-12 col-md-8 mt-4">
        <?php
          if($scriptIframe != '') {
            ?>
            <div class="embed-container">
              <?php echo $scriptIframe; ?>
            </div>
          <?php
            } else {
            echo '<img src="'. $imagenStreaming["url"] .'" width="100%">';
          }
          ?>

      </div>

    </div>
  </div>


<div class="container mt-5">
  <div class="row justify-content-center mb-2"><div class="col-12 text-center"><div class="tituloSeccion"><?php echo $tituloSeccion; ?></div></div></div>
  <div class="row justify-content-center">
    <?php echo $contPonentes; ?>

  </div>
</div>
