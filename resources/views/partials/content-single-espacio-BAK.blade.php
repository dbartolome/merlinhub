<article class="contenedor d-flex flex-wrap">
  <div class="col-izq">

    <header class="d-flex flex-wrap mb-5 mb-lg-2 p-4">
      <h1 class="entry-title text-principal order-1 order-sm-0">{!! get_the_title() !!}</h1>
      <nav class="nav-links order-0 order-sm-1">
        {{ previous_post_link('%link', '<span>&laquo;</span> %title') }} {{ next_post_link('%link', '%title <span>&raquo;</span>') }}
      </nav>
    </header>

    <section class="entry-content p-4">
      @php the_content() @endphp
    </section>

    <section class="p-4 iconos d-flex flex-wrap align-items-start justify-content-center">

      @hasfield('espacios_superficie')
        <article>
          @svg('icono-superficie')
          <ul>
            <li>
              @field('espacios_superficie') m<sup>2</sup>
            </li>
          </ul>
        </article>
      @endfield

      @hasfield('espacios_personas')
        <article>
          @svg('icono-personas')
          <ul>
            <li>
              @field('espacios_personas') personas
            </li>
          </ul>
        </article>
      @endfield

      @hasfield('espacios_empresas')
        <article>
          @svg('icono-empresas')
          <ul>
            <li>
              @field('espacios_empresas') empresas
            </li>
          </ul>
        </article>
      @endfield

      @if (get_field('espacios_loom'))
        <article>
          @svg('icono-loom')
          @if (get_field('espacios_flexspaces'))
          <ul>
            <li>
              Flexspaces by loom
            </li>
          </ul>
          @endif
        </article>
      @endif

      @hasfield('espacios_espacios_comunes')
        <article>
          @svg('icono-comunes')
          <ul>
            @foreach ($espacios_comunes as $item)
              <li>{{ $item }}</li>
            @endforeach
          </ul>
        </article>
      @endfield

      @hasfield('espacios_wellness')
        <article>
          @svg('icono-wellness')
          <ul>
            @foreach ($espacios_wellness as $item)
              <li>{{ $item }}</li>
            @endforeach
          </ul>
        </article>
      @endfield

      @hasfield('espacios_restaurantes')
        <article>
          @svg('icono-restaurantes')
          <ul>
            @foreach ($espacios_restaurantes as $item)
              <li>{{ $item }}</li>
            @endforeach
          </ul>
        </article>
      @endfield

      @hasfield('espacios_servicios')
        <article>
          @svg('icono-servicios')
          <ul>
            @foreach ($espacios_servicios as $item)
              <li>{{ $item }}</li>
            @endforeach
          </ul>
        </article>
      @endfield

      @hasfield('espacios_comunidad')
        <article>
          @svg('icono-comunidad')
          <ul>
            @foreach ($espacios_comunidad as $item)
              <li>{{ $item }}</li>
            @endforeach
          </ul>
        </article>
      @endfield

    </section>

    @hasfield('espacios_microsite_url')
      <section class="microsite p-4 d-flex justify-content-center">
        <a href="@field('espacios_microsite_url')" target="_blank" class="px-3 py-2">Ver microsite</a>
      </section>
    @endfield

    @hasfield('espacios_certificados')
      <section class="calidad p-4 d-flex flex-wrap justify-content-center">
        @foreach ($espacios_cert as $cert)
            <div class="cert p-4">
              <img src="@asset($cert)">
            </div>
        @endforeach
      </section>
    @endfield
  </div>


    <section class="galeria col-der">
      @hasfield('espacios_imagenes_galeria')
          <div class="glide">

            <div class="glide__track" data-glide-el="track">
              <ul class="glide__slides">
                @fields('espacios_imagenes_galeria')
                  <li class="glide__slide">
                    <img src="@sub('espacios_imagen_galeria', 'url')" alt="@sub('espacios_imagen_galeria', 'alt')" />
                  </li>
                @endfields
              </ul>
            </div>

            <div class="glide__arrows" data-glide-el="controls">
              <button class="glide__arrow glide__arrow--left" data-glide-dir="<">
                @svg('flecha-slider-izq')
              </i></button>
              <button class="glide__arrow glide__arrow--right" data-glide-dir=">">
                @svg('flecha-slider-der')
              </button>
            </div>

            <div class="glide__bullets" data-glide-el="controls[nav]">
              @for ($i = 0; $i < $glide_bullets; $i++)
            <button class="glide__bullet" data-glide-dir="={{ $i }}"></button>
              @endfor
            </div>

          </div>
      @endfield

    </section>

</article>

