<div class="page-header container-fluid">
  <div class="row">
    @thumbnail('full')
    @if (!is_page(array( 'contacto', 'contact')))
      <div class="wrapper col px-md-0 col-md-11 offset-md-1">
        <h1 class="text-principal mt-5">{!! App::title() !!}</h1>
      </div>
    @endif
  </div>
</div>
