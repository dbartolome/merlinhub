<footer id="footer-territorio" class="content-info">
        <div class="logoFooter">
          <?php
          $image = get_field('logo_footer', 'option');
          $size = "100%";
          if( $image ) {
            $output = '<img src="'.$image['url'].'" alt="'. $image['title'].'" width="'. $size .'" class="logo_footer">';
          }

          echo $output;
          ?>
        </div>
        @php dynamic_sidebar('sidebar-footer') @endphp

      @php dynamic_sidebar('bottom-footer') @endphp

          @php dynamic_sidebar('newsletter-footer') @endphp

          @php dynamic_sidebar('legales-footer') @endphp

</footer>
