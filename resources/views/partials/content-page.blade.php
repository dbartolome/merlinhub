<div class="container-fluid">
  <div class="row">
    <div class="col-md-10 offset-md-1 px-md-0">
      @php the_content() @endphp
    </div>
  </div>
</div>
