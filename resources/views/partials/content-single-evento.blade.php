<article @php post_class() @endphp>

  <div class="container">
    <div class="row">
      <div class="d-none col-lg-1 d-lg-block"></div>
      <div class="col-12 col-lg-11"><div class="row">
          <header class="d-flex flex-wrap mb-5 mb-lg-2 p-2">
            <nav class="nav-links order-0 order-sm-1">
              <?php
              $my_current_lang = apply_filters( 'wpml_current_language', NULL );

              if ($my_current_lang == 'es') {
              ?>  {{ previous_post_link('%link', '<span>&laquo;</span> evento anterior') }}  {{ next_post_link('%link', 'evento siguiente <span>&raquo;</span>') }}<?php
              } elseif ($my_current_lang == 'en') {
              ?>{{ previous_post_link('%link', '<span>&laquo;</span> PREVIOUS EVENT') }}  {{ next_post_link('%link', 'next event <span>&raquo;</span>') }} <?php
              }
              ?>

            </nav>
          </header>
        </div>
        <div class="row">

          <div class="col-11">
            <div class="headerEvento">
           <!-- <div class="tipoEvento">@term('tipo')</div>
              <h2 class="entry-title">{!! get_the_title() !!}</h2> -->
            </div>
            <div class="contenidoEvento">
             @php the_content() @endphp
            </div>
          </div>
        </div>


      </div>
    </div>
  </div>

</article>

