<div class="container-fluid">
  <div class="row justify-content-center py-5">
    <div class="col-12 col-sm-10 col-lg-9 col-xl-8 col-dxl-7">
      @php the_content() @endphp
    </div>
  </div>
</div>
