<div  style="background-image: url(<?php echo get_the_post_thumbnail_url(get_the_ID(),'full'); ?>); background-size: cover; background-position: center; height: 350px;"></div>
<div class="page-header" >
  <div class="container">
    <div class="row">

      <div class="col-12 col-lg-11">
        <div class="row">
          <div class="col-12">
            <h1 class="text-principal mt-3">{{the_title()}}</h1>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
