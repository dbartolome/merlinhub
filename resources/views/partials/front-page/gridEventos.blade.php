<?php
$restoArticulos = '';
$eventosHome = get_field('eventosHome','options');

if( $eventosHome ):
  foreach( $eventosHome as $evento ):

    // Setup this post for WP functions (variable must be named $post).
    $unixtimestamp = strtotime( get_field('fechaEvento',$evento->ID) );
    $anho = intval(date_i18n( "Y", $unixtimestamp ));
    $mes = date_i18n( "F", $unixtimestamp );
    $dia = intval(date_i18n( "d", $unixtimestamp ));

    $permalink = get_permalink( $evento->ID );
    $title = get_the_title( $evento->ID );
    $campoCargo = get_field( 'cargo', $evento->ID );
    $imageEvento = get_field( 'imagen_repo', $evento->ID );
    $textoArticulo = get_field('texto_introduccion', $evento->ID);
    setup_postdata($evento);

    $restoArticulos .='<div class="col-12 col-sm-6 col-md-4 p-2 art">
                                <a href="'. $permalink .'" role="article" style="color: #fff">
                                <div class="articulo">
                                    <div  class="capaTextoEvento">
                                        <div class="fechaEvento">'. $dia .'  '.$mes.'</div><div class="anhoFecha">'. $anho .'</div>
                                       <h2 class="entry-title" style="margin: 9% 0;">
                                        '. $title .'
                                       </h2>
                                        <div class="entry-content">
                                            '. $textoArticulo  .'
                                        </div>

                                    </div>
                                    <div class="fondo" style="background-image: url('.$imageEvento['url'].');"></div></div>
                                </a>
                                </div> ';



  endforeach;
  wp_reset_postdata();

endif;




?>






    <?php echo $restoArticulos ;?>

