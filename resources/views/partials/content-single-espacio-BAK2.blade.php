<article class="contenedor d-flex flex-wrap">

  <section class="galeria col-der">
    @hasfield('espacios_imagenes_galeria')
        <div class="glide">

          <div class="glide__track" data-glide-el="track">
            <ul class="glide__slides">
              @fields('espacios_imagenes_galeria')
                @php $id = get_sub_field('espacios_imagen_galeria')['id'] @endphp
                @component('components.bg-image-slider', ['image' => $id]) @endcomponent
              @endfields
            </ul>
          </div>

          <div class="glide__arrows" data-glide-el="controls">
            <button class="glide__arrow glide__arrow--left" data-glide-dir="<">
              @svg('flecha-slider-izq')
            </i></button>
            <button class="glide__arrow glide__arrow--right" data-glide-dir=">">
              @svg('flecha-slider-der')
            </button>
          </div>

          <div class="glide__bullets" data-glide-el="controls[nav]">
            @for ($i = 0; $i < $glide_bullets; $i++)
          <button class="glide__bullet" data-glide-dir="={{ $i }}"></button>
            @endfor
          </div>

        </div>
    @endfield
  </section>

</article>

