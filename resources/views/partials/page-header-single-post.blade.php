<div class="page-header">
  @hasfield('noticia_header_img')
      <img src="@field('noticia_header_img', 'url')" alt="@field('noticia_header_img', 'alt')" />
  @endfield
  <div class="container">
    <div class="row">
      <div class="col-md-9 offset-md-1 px-md-0">
  <h1 class="text-principal mt-3">{!! App::title() !!}</h1>
      </div>
    </div>
  </div>
</div>
