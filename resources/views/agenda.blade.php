{{--
Template Name: Agenda
--}}

@extends('layouts.app')

@section('content')


@while(have_posts()) @php the_post() @endphp
@include('partials.page-header')

<div class="container">
  <div class="row justify-content-center">
    @include('partials.content-page')
    <div class="infinite-scroll-container">
      @query([
      'post_type' => 'evento'
      ])
      @posts
      @include('partials.content-'.get_post_type())
      <a href="{{ get_permalink() }}" role="article" class="art d-flex my-4 px-sm-4">
        @thumbnail("large")
        <div class="textos">
          <h2 class="entry-title pb-1 mb-2">@title</h2>
          <div class="entry-summary">
            @excerpt
          </div>
        </div>
      </a href="" role="article">
      @endposts
    </div>
  </div>
</div>
@endwhile

<nav class="nav-links">
  <div class="next-link">
    {!! next_posts_link( 'Older Entries', $query->max_num_pages ) !!}
  </div>
  <div class="previous-link">
    {!! previous_posts_link( 'Newer Entries' )!!}
  </div>
</nav>
<div class="container">
  <div class="row">
    @include('partials.loader')
  </div>
</div>



@endsection

