<!doctype html>
<html {!! get_language_attributes() !!}>
  @include('partials.head')
  <body @php body_class() @endphp>
  <script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "Organization",
    "name": "Merlin Hub",
    "url": "https://merlinhub.es",
    "address": "Paseo de la Castellana, 257, 28046 Madrid",
    "sameAs": [
      "https://www.facebook.com/MerlinHubMadridNorte",
      "https://www.instagram.com/merlinhub/",
      "https://www.linkedin.com/company/merlin-hub/?viewAsMember=true"
    ]
  }
</script>
  <?php if ( function_exists( 'gtm4wp_the_gtm_tag' ) ) { gtm4wp_the_gtm_tag(); } ?>
    @php do_action('get_header') @endphp
    @include('partials.header')
    <div class="wrap" role="document">
      <div class="content">
        <main class="main">
          @yield('content')
        </main>
        <!-- @if (App\display_sidebar())
          <aside class="sidebar">
            @include('partials.sidebar')
          </aside>
        @endif -->
       </div>
    </div>

    <?php
    if (is_singular('territorio')){
    ?>
        @php do_action('get_footer') @endphp
        @include('partials.footer-territorio')
        @php wp_footer() @endphp
    <?php } else { ?>
        @php do_action('get_footer') @endphp
        @include('partials.footer')
        @php wp_footer() @endphp
  <?php } ?>
  </body>
</html>
