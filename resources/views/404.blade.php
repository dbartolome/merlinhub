{{--
  Template Name: Template 404
--}}
@extends('layouts.app')

@section('content')
  @include('partials.page-header')

  @if (!have_posts())
    <div class="alert alert-warning">
      {{ __('Lo sentimos.', 'sage') }}
    </div>
    {!! get_search_form(false) !!}
    @include('partials.content-page')
  @endif
@endsection
