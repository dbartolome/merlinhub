{{--
  Template Name: Template para event brite
--}}

@extends('layouts.app')

@section('content')
<?php
  $args = array(
  'post_type'=>'eventbrite_events',
  'posts_per_archive_page' => -1,
    'facetwp' => true, // we added this
    'meta_key' => 'start_ts',
    'orderby' => array(
        'meta_key',
    ),
    'order' => 'ASC',
  );
  $loop = new WP_Query($args);
  ?>
@include('partials.page-header')
<div class="container">
<div class="row">
  <div class="col-12"></div>

</div>



  <div class="row">

    <div class="col-11">
      <div class="row">
      @while($loop->have_posts()) @php $loop->the_post() @endphp
        @include('partials.content-eventbrite_events')
      @endwhile
      </div>
    </div>
  </div>
</div>


@endsection
