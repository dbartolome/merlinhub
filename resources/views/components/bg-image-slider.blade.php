<li class="glide__slide c-bg-image {{$class ?? ''}} js-bg-image-lg">
  <img class="d-lg-none" src="{{ wp_get_attachment_image_src($image, 'full')[0] }}" sizes="(max-width: 992px) 100vw, 40vw" srcset="{{ wp_get_attachment_image_srcset($image, 'full') }}">
  {!! $slot !!}
</li>
