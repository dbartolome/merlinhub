<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class SingleTerritorio extends Controller
{
    public function mostrarLayout()
    {
        $contTotal = '';
        if (have_rows('contenedores')) {
            $i = 0;
            $j = 0;
            $anchoTotal = 0;
            $numCol = 0;
            while (have_rows('contenedores')) {
                the_row();
                if (get_row_layout() == 'layout1') {
                    $anchoBloqueUno = get_sub_field('tamanoLayout');
                    $contLayoutUno = '';
                    $text = get_sub_field('titulo_espacio');
                    $contEspacio = get_sub_field('contenido_espacio');
                    $contLayoutUno .= '<div class="box txtTituloCont" style="width: ' . $anchoBloqueUno . 'vw;">';
                    $contLayoutUno .= '<div class="contTexto"><div class="tituloSeccion">';
                    $contLayoutUno .= $text;
                    $contLayoutUno .= '</div>';
                    $contLayoutUno .= '<hr class="lineaIzquierda">';
                    $contLayoutUno .= '<div class="contSeccion">';
                    $contLayoutUno .= $contEspacio;
                    $contLayoutUno .= '</div>';
                    $contLayoutUno .= '</div>';
                    $contLayoutUno .= '</div>';
                    $contTotal .= $contLayoutUno;


                }
                else if (get_row_layout() == 'layout2') {
                    $contLayoutDos = '';
                    $anchoBloqueDos = get_sub_field('tamanoLayoutDos');
                    $image = get_sub_field('imagen_bloque_dos');
                    $imagenFondo = get_sub_field('imagen_fondo');
                    if ($imagenFondo == 0) {
                        $contLayoutDos .= '<div class="box imagenCont" style="width: ' . $anchoBloqueDos . 'vw;">';
                        $contLayoutDos .= '<div class="conImagen">';
                        $contLayoutDos .= '<img src="' . $image['url'] . '" class="img-parallax"></div>';
                        $contLayoutDos .= '</div>';
                    } else if ($imagenFondo == 1) {
                        $i++;
                        $contLayoutDos .= '<div class="box parallax parallax'.$i.' " style="width: ' . $anchoBloqueDos . 'vw;" data-movimiento="5">';
                        //$contLayoutDos .= '<div class="box parallax parallax'.$i.' " style="width: ' . $anchoBloqueDos . 'vw;" data-movimiento="5"><div class="imgFondo" style="background-image: url(' . $image['url'] . ');" >';
                        $contLayoutDos .= '<img src="' . $image['url'] . '" class="imgFondo">';
                        $contLayoutDos .= '</div>';
                        //$contLayoutDos .= '</div></div>';
                    }

                    $contTotal .= $contLayoutDos;
                }
                else if (get_row_layout() == 'layout3') {
                    $contLayoutTres = '';
                    $contEspacio = get_sub_field('contenido_espacio');
                    $anchoBloqueTres = get_sub_field('tamanoLayoutTres');
                    $contLayoutTres .= '<div class="box editorCont" style="width: ' . $anchoBloqueTres . 'vw;">';
                    $contLayoutTres .= '<div class="contTexto">';
                    $contLayoutTres .= $contEspacio;
                    $contLayoutTres .= '</div>';
                    $contLayoutTres .= '</div>';
                    $contTotal .= $contLayoutTres;
                    $j++;
                }
                else if (get_row_layout() == 'layout4') {
                    $contLayoutCuatro = '';
                    $contEspacio = get_sub_field('contenido_espacio');
                    $anchoBloqueCuatro = get_sub_field('tamanoLayoutCuatro');
                    $contLayoutCuatro .= '<div class="box txtContSF" style="width: ' . $anchoBloqueCuatro . 'vw;">';
                    $contLayoutCuatro .= $contEspacio;
                    $contLayoutCuatro .= '</div>';
                    $contTotal .= $contLayoutCuatro;

                }
                else if (get_row_layout() == 'layout5') {
                    $contLayoutCinco = '';
                    $anchoBloqueCinco = get_sub_field('tamanoLayoutCinco');
                    $contLayoutCinco .= '<div class="box separadorCont" style="width: ' . $anchoBloqueCinco . 'vw;">';
                    $contLayoutCinco .= '</div>';
                    $contTotal .= $contLayoutCinco;

                }
                else if (get_row_layout() == 'layout6') {
                    $contLayoutSeis = '';
                    $contEspacio = '<div class="container"><div class="row align-items-center altoTotal"><div class="col-12 conBorde"><div class="sepVerticalUno"></div><div class="sepVerticalDos"></div><div class="sepVerticalTres"></div><div class="row" >';
                    $anchoBloqueSeis = get_sub_field('tamanoLayoutSeis');
                    if( have_rows('gridTerritorio') ):
                        $i = 0;
                        // loop through the rows of data
                        while ( have_rows('gridTerritorio') ) : the_row();
                            // display a sub field value
                            $textoGrid = get_sub_field('textGrid');
                            $image2 = get_sub_field('imagenGrid');
                            $textSVG = get_sub_field('codigoSVG');
                            $contEspacio .= '<div class="col-6 col-lg-3 gridCont">';
                            if($textSVG != '') {
                                $contEspacio .= '<div class="imgGrid">' . $textSVG . '</div>';
                            } else {
                                $contEspacio .= '<div class="imgGrid"><img src="' . $image2['url'] . '" width="100%"></div>';
                            }
                            $contEspacio .= '<div class="txtGrid">'. $textoGrid.'</div>';
                            $contEspacio .= '</div>';
                            if($i==3) {
                                $contEspacio .= '<div class="separador"></div>';
                            }
                            $i++;
                        endwhile;

                    else :

                        // no rows found

                    endif;
                    $contEspacio .= "</div></div></div></div>";
                    $contLayoutSeis .= '<div class="box gridTerritorio" style="width: ' . $anchoBloqueSeis . 'vw;">';
                    $contLayoutSeis .= $contEspacio;
                    $contLayoutSeis .= '</div>';
                    $contTotal .= $contLayoutSeis;

                }
                else if (get_row_layout() == 'layout7') {
                    $contLayoutSiete = '';
                    $contEspacio = get_sub_field('contenido_espacio');
                    $anchoBloqueSiete = get_sub_field('tamanoLayoutSiete');
                    $contLayoutSiete .= '<div class="box" style="width: ' . $anchoBloqueSiete . 'vw;">';
                    $contLayoutSiete .= $contEspacio;
                    $contLayoutSiete .= '</div>';
                    $contTotal .= $contLayoutSiete;

                }
                else if (get_row_layout() == 'layout8') {
                    $contLayoutOcho = '';
                    $contEspacio = '<div class="container"><div class="row align-items-center altoTotal"><div class="col-12 sinBorde"><div class="row justify-content-center" >';
                    $anchoBloqueOcho = get_sub_field('tamanoLayoutOcho');
                    if( have_rows('gridTerritorioSinLineas') ):
                        // loop through the rows of data
                        while ( have_rows('gridTerritorioSinLineas') ) : the_row();

                            // display a sub field value
                            $textoGrid = get_sub_field('textGrid');
                            $image3 = get_sub_field('imagenGrid');
                            $titGrid = get_sub_field('TitularGrid');
                            $textSVG = get_sub_field('codigoSVG');
                            $nombreCapa = get_sub_field('nomCapa');

                            $contEspacio .= '<div class="col-6 col-lg-3 gridContSB mb-3 columna'. $numCol .'">';
                            if($textSVG != '') {
                                $contEspacio .= '<div class="line-drawing-demo">' . $textSVG . '</div>';
                            } else {
                                $contEspacio .= '<div class="imgGrid"><img src="' . $image3['url'] . '" width="100%"></div>';
                            }
                            $contEspacio .= '<div class="titGridSB">'. $titGrid .'</div>';
                            $contEspacio .= '<div class="txtGridSB">'. $textoGrid.'</div>';
                            $contEspacio .= '</div>';
                            $numCol++;
                        endwhile;

                    else :

                        // no rows found

                    endif;
                    $contEspacio .= "</div></div></div></div>";
                    $contLayoutOcho .= '<div class="box gridTerritorioSB " style="width: ' . $anchoBloqueOcho . 'vw;">';
                    $contLayoutOcho .= $contEspacio;
                    $contLayoutOcho .= '</div>';
                    $contTotal .= $contLayoutOcho;

                }
                else if (get_row_layout() == 'layout9') {
                    $contLayoutNueve = '';
                    $contEspacio = get_sub_field('contenido_espacio');
                    $anchoBloqueNueve = get_sub_field('tamanoLayoutNueve');
                    $contLayoutNueve .= '<div class="box contMapa" style="width: ' . $anchoBloqueNueve . 'vw;">';
                    $contLayoutNueve .= $contEspacio;
                    $contLayoutNueve .= '</div>';
                    $contTotal .= $contLayoutNueve;

                }
                else {

                }
            }

            return $contTotal;

        }
    }

}

