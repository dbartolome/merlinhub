<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class SingleEvento extends Controller
{
    public function fechaEvento()
    {
        function cambiarMes($mes) {
            switch ($mes) {

                case 'January':
                    $mesTraducido = "Enero";
                    break;
                case 'Februaryy':
                    $mesTraducido = "Febrero";
                    break;
                case 'March':
                    $mesTraducido = "Marzo";
                    break;
                case 'April':
                    $mesTraducido = "Abril";
                    break;
                case 'May':
                    $mesTraducido = "Mayo";
                    break;
                case 'June':
                    $mesTraducido = "Junio";
                    break;
                case 'July':
                    $mesTraducido = "Julio";
                    break;
                case 'Agost':
                    $mesTraducido = "Agosto";
                    break;
                case 'September':
                    $mesTraducido = "Septiembre";
                    break;
                case 'October':
                    $mesTraducido = "Octubre";
                    break;
                case 'November':
                    $mesTraducido = "Noviembre";
                    break;
                case 'December':
                    $mesTraducido = "Diciembre";
                    break;
            }
            return $mesTraducido;
        }

        global $post;
        $output = '';
        $diaEvento = '';
        $horaEvento = '';
        $mesEvento = '';
        if( have_rows('fecha_evento', $post->ID )):

            // loop through the rows of data

            while ( have_rows('fecha_evento', $post->ID)) : the_row();


                // display a sub field value
                // the_sub_field('sub_field_name');
                $varDia = get_sub_field('dia_evento');
                $diaEvento = date("d", strtotime($varDia));
                $diaEvento .= '<br />';
                $mesEvento = date('F', strtotime($varDia));
                $mesEvento = cambiarMes($mesEvento);
                //$horaEvento .= '<div class="horaEvento">';
                //$horaEvento .= ' de ';
                //$horaEvento .= get_sub_field('hora_comienzo');
                //$horaEvento .= ' a ';
                //$horaEvento .= get_sub_field('hora_final');
                //$horaEvento .= '</div>';
                $output .= '<div class="diaEvento">'. $diaEvento .'</div>';
                $output .= '<div class="mesEvento">'. $mesEvento .'</div>';
                $diaEvento = '';
                $output .= $horaEvento;
                $horaEvento .= '<div class="horaEvento">'. get_sub_field('horarioEvento') .'</div>';


            endwhile;
            $output .= '<div class="diaEvento">'. $diaEvento .'</div>';
            $diaEvento = '';
            $output .= $horaEvento;
            $horaEvento = '';

        else :

            // no rows found

        endif;


        return $output;
    }

    public function lugarEvento() {
        global $post;
        $lugares = get_field('lugar_evento', $post->ID);
        // print_r($lugares);
        $output = '';
        if( $lugares ):
            foreach( $lugares as $lugar): // variable must be called $post (IMPORTANT)
                $urlLugar = get_the_permalink($lugar->ID);
                setup_postdata($lugar);
                $output .= ' <div class="col-12 col-lg-11 col-xxl-10 text-center m-3"><a href="'. $urlLugar .'" class="linkLugarEvento">'.  get_the_title($lugar->ID) .'</a></div>';
                $mapa =  get_field('mapa_espacio', $lugar->ID);
                $output .= '<div class="col-12 col-lg-11 col-xxl-10">'. do_shortcode($mapa).'</div>';



            endforeach;
            wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly
        endif;

        return $output;
    }
}
