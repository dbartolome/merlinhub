<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class noticias extends Controller
{

    public function argsNoticias() {
        $args_news = [
        	'post_type'              => 'post',
        	'post_status'            => 'publish',
            'posts_per_page'         => 5,
            'paged'=> get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1,
        ];
        return $args_news;
    }
}
