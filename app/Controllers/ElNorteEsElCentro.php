<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class ElNorteEsElCentro extends Controller
{

    public function eneecMapa1() {

        $img = get_field('eneec_mapa1');
        $img_srcset = wp_get_attachment_image_srcset($img['ID']);

        return '<img
            class="img w-100 my-4"
            src=" ' . $img['url'] . ' "
            alt=" ' . $img['title'] . ' "
            srcset=" ' . $img_srcset . ' "
            sizes="100vw"
        >';

    }

    public function eneecMapa2() {

        $img = get_field('eneec_mapa2');
        $img_srcset = wp_get_attachment_image_srcset($img['ID']);

        return '<img
            class="img w-100"
            src=" ' . $img['url'] . ' "
            alt=" ' . $img['title'] . ' "
            srcset=" ' . $img_srcset . ' "
            sizes="(max-width: 768px) 100vw, 60vw"
        >';

    }

    public function eneecImg1() {

        $img = get_field('eneec_img1');
        $img_srcset = wp_get_attachment_image_srcset($img['ID']);

        return '<img
            class="img w-100"
            src=" ' . $img['url'] . ' "
            alt=" ' . $img['title'] . ' "
            srcset=" ' . $img_srcset . ' "
            sizes="(max-width: 768px) 100vw, 60vw"
        >';

    }

    public function eneecImg2() {

        $img = get_field('eneec_img2');
        $img_srcset = wp_get_attachment_image_srcset($img['ID']);

        return '<img
            class="img w-100"
            src=" ' . $img['url'] . ' "
            alt=" ' . $img['title'] . ' "
            srcset=" ' . $img_srcset . ' "
            sizes="(max-width: 768px) 100vw, 60vw"
        >';

    }

    public function eneecImg3() {

        $img = get_field('eneec_img3');
        $img_srcset = wp_get_attachment_image_srcset($img['ID']);

        return '<img
            class="img w-100"
            src=" ' . $img['url'] . ' "
            alt=" ' . $img['title'] . ' "
            srcset=" ' . $img_srcset . ' "
            sizes="(max-width: 768px) 100vw, 60vw"
        >';

    }
}
