<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class ArchiveEvento extends Controller
{
    public function lugarEvento() {

        $lugares = get_field('lugar_evento');

        $output = '';
       // print_r($lugares);
        if( $lugares ):
            $output .= '<a href="'. $lugares[0]->guid .'" class="linkLugarEvento">'.  $lugares[0]->post_title .'</a>';
        endif;
        return $output;
    }
}
