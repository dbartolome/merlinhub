<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class PageTemplateMerlinHubApp extends Controller

{

    public static function mhappIcon() {

        $img = get_sub_field('mhapp_icono');

        switch ($img) {
            case 'control':
                $output = get_template_directory_uri() . '/svg/icono-control-accesos.svg';
                break;
            case 'movilidad':
                $output = get_template_directory_uri() . '/svg/icono-movilidad.svg';
                break;
            case 'ofertas':
                $output = get_template_directory_uri() . '/svg/icono-ofertas.svg';
                break;
            case 'food':
                $output = get_template_directory_uri() . '/svg/icono-food.svg';
                break;
            case 'noticias':
                $output = get_template_directory_uri() . '/svg/icono-noticias.svg';
            break;
            case 'transporte':
                $output = '/wp-content/themes/merlinhub/dist/images/IconoMovilidad.png';
                break;
        }
        return $output;

    }
}
