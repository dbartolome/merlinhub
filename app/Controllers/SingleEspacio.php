<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class SingleEspacio extends Controller
{
    public function direccionEspacio() {

        return get_field('direccion_espacio');

    }
    public function ciudadEspacio() {

        return get_field('ciudad_espacio');

    }

    public function espaciosComunes() {

        return get_field('espacios_espacios_comunes');

    }

    public function espaciosWellness() {

        return get_field('espacios_wellness');

    }

    public function espaciosRestaurantes() {

        return get_field('espacios_restaurantes');

    }

    public function espaciosServicios() {

        return get_field('espacios_servicios');

    }

    public function espaciosComunidad() {

        return get_field('espacios_comunidad');

    }

    public function glideBullets() {

        return count(get_field('espacios_imagenes_galeria'));

    }

    public function espaciosCert() {

        $output = [];

        $certs = get_field('espacios_certificados');

        if (in_array('leed', $certs)) {
            $output[] = 'images/certs/calidad-leed.jpg';
        }

        if (in_array('wired_score', $certs)) {
            $output[] = 'images/certs/calidad-wired-score.jpg';
        }

        if (in_array('diga', $certs)) {
            $output[] = 'images/certs/calidad-diga.jpg';
        }

        if (in_array('breeam', $certs)) {
            $output[] = 'images/certs/calidad-breeam.jpg';
        }


        return $output;

    }

}
