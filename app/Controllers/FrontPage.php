<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class FrontPage extends Controller
{
    /* aqui comienza el desarrollo para el destacado principal */
    public function imagenFondo()
    {
        $output = '';
        $image = get_field('fondo_destacado', 'option');
        if( $image ) {
            $output = $image['url'];
        }

        return $output;
    }

    /* aqui comienza el desarrollo para el destacado principal */

    public function tituloDestacado()
    {
        $output = get_field('titular_destacado', 'option');
        return $output;
    }

    public function textoDestacado()
    {
        $output = get_field('texto_destacado', 'option');
        return $output;
    }

    public function enlaceDestacado()
    {
        $varLink = get_field('enlace_destacado', 'option');
        $txtLink = get_field('texto_enlace_destacado', 'option');
        $output = '<a href="';
        $output .= $varLink;
        $output .='" class="linkLeermsB"><span class="verde">+</span>';
        $output .= $txtLink;
        $output .='</a>';
        return $output;
    }

    /* aqui comienza el desarrollo para el destacado del programa */

    public function programaApp() {
        // Check rows exists.
        if (have_rows('destacadoDos','option')):
            $outputGeneral = '';
            // Loop through rows.
            while (have_rows('destacadoDos', 'option')) : the_row();

                // Load sub field value.
                $imageFondo = get_sub_field('imagen_programa');
                $tituloDestacado = get_sub_field('titular_programa');
                $textDestacado = get_sub_field('texto_programa');
                $varLink = get_sub_field('enlace_programa');
                $txtLink = get_sub_field('texto_enlace_programa');
                $output = '<a href="';
                $output .= $varLink;
                $output .='" class="linkLeermsB"><span class="verde">+</span>';
                $output .= $txtLink;
                $output .='</a>';
                // Do something...

                $outputGeneral .= '<div class="row sinPadding mt-2">
                                        <div class="col-12 col-lg-6 capaIzquierda sinPadding">
                                            <div class="capaTexto">
                                                <h2 class="titContPrograma" style="color: #fff">'. $tituloDestacado .'</h2>
                                                <p>'. $textDestacado .'</p>
                                                    '. $output .'
                                            </div>
                                        </div>
                                        <div class="col-12 col-lg-6 imagenPrograma sinPadding"><img src="'.$imageFondo['url'].'" alt="'. $imageFondo['title'].'"></div>
                                    </div>';


                // End loop.
            endwhile;

        // No value.
        else :
            // Do something...
        endif;

        return $outputGeneral;
    }

    /* aqui comienza el desarrollo para el destacado del territorio */

    public function crearTerritorio()
    {
        if (have_rows('territorio','option')) {
            $contTotal = '';
            while (have_rows('territorio','option')){

                the_row();
                // vars
                $image = get_sub_field('imagen_territorio');
                $urlImagen = $image['url'];
                $altImagen = $image['name'];
                $texto = get_sub_field('titular_territorio');
                $content = get_sub_field('texto_territorio');
                $link = get_sub_field('enlace_territorio');
                $textLink = get_sub_field('texto_enlace_territorio');
                $posicion = get_sub_field('posicionimagen');

                $capaDer = '';
                $capaIz='';
                if($posicion == 'derecha') {
                    $capaDer .= '<a href="'. $link . '" >
                                            <div class="row align-items-center separacion sinPadding">
                                            <div class="d-none d-xl-block col-xl-1 order-xl-1"></div>
                                            <div class="col-12 order-2 col-lg-6 order-lg-1 col-xl-4 alignderecha sinPadding"><h2>'. $texto .' </h2><hr class="lineaDerecha"><p>'. $content. '</p><div class="linkLeermas"><span class="verde">+</span>'. $textLink. '</div></div>
                                            <div class="d-none d-xl-block col-xl-1 order-xl-3"></div>
                                            <div class="col-12 order-1 col-lg-6 order-lg-4 col-xl-6 sinPadding"><img src="'.  $urlImagen .'" width="100%" alt="'. $altImagen .'"></div></div></a>';
                    $contTotal .= $capaDer;

                } else{
                    $capaIz .= '<a href="'. $link .'">
                                        <div class="row align-items-center separacion sinPadding">
                                        <div class="col-12 col-lg-6 col-xl-6 sinPadding"><img src="' . $urlImagen .'" width="100%" alt="'. $altImagen .'"></div>
                                        <div class="d-none d-xl-block col-xl-1"></div>
                                        <div class="col-12 col-lg-6 col-xl-4 sinPadding"><h2>'. $texto .'</h2><hr class="lineaIzquierda"><p>'. $content. '</p><div class="linkLeermas"><span class="verde">+</span>'. $textLink. '</div></div>
                                        <div class="d-none d-xl-block col-xl-1"></div></div></a>';
                    $contTotal .= $capaIz;


                }
            }
        }

        return $contTotal;


    }

    /* aqui comienza el desarrollo para la Explora */
    public function tituloSeccionExplora()
    {
        $output = get_field('titulo_seccion_explora', 'option');
        return $output;
    }
    public function codigoSeccionExplora()
    {
        $output = '';

        $output = get_field('codigo-Explora', 'option');


        return $output;
    }

    public function botonVerEspacios () {
        $contTotal = '';
        $textLink = get_field('texto_link_explorar','option');
        $linkEspacios = get_field('link_explorar','option');
        $contTotal .= '<div class="button-container"><a href="'. $linkEspacios .'" class="view-more-button boton">';
        $contTotal .= $textLink;
        $contTotal .= '</a></div>';
        return $contTotal;

    }

    /* aqui comienza el desarrollo para la noticia destacada */

    public function tituloSeccionNoticia()
    {
        $output = get_field('titulo_seccion_noticia', 'option');
        return $output;
    }

    public function imagenNoticia()
    {
        $output = '';

        $image = get_field('imagen_noticia', 'option');
        $size = "100%";
        if( $image ) {
            $output = '<img src="'.$image['url'].'" alt="'. $image['title'].'" width="'. $size .'">';
        }

        return $output;
    }

    public function tituloNoticia()
    {
        $output = get_field('titular_noticia', 'option');
        return $output;
    }

    public function txtNoticia()
    {
        $output = get_field('txt_noticia', 'option');
        return $output;
    }

    public function enlaceNoticia()
    {
        $txtLink = get_field('texto_enlace_programa', 'option');
        $output = '<div class="linkLeermas"><span class="verde">+</span>';
        $output .= $txtLink;
        $output .='</div>';
        return $output;
    }

    /* aqui comienza el desarrollo para la AGENDA */
    public function tituloSeccionAgenda()
    {
        $output = get_field('titulo_seccion_agenda', 'option');
        return $output;
    }

    public function verAgenda() {

        $i = 0;
        $mi_var = array();

        if( have_rows('evento','option') ) {

            // loop through the rows of data
            while (have_rows('evento','option')) {
                the_row();
                // display a sub field value
                $mi_var[$i] = get_sub_field('select_event');
                $i++;
            }
        } else {
            // no rows found
        }

        $fechaHoy = date("y-m-d");
        $random_posts = get_posts(
            [
                'post_type'=>'evento',
                'posts_per_archive_page' => 3,
                'suppress_filters' => false,
                'meta_key'			=> 'fecha_comienzo',
                'orderby'			=> 'meta_value',
                'order'				=> 'ASC',
                'meta_query'  => array(
                array(
                    'key'     => 'fecha_comienzo',
                    'value'   => $fechaHoy,
                    'compare' => '>=',
                    'type'    => 'DATE'
                ),
            )
            ]
        );
        function cambiarMes($mes) {
            switch ($mes) {

                case 'January':
                    $my_current_lang = apply_filters( 'wpml_current_language', NULL );
                    if ($my_current_lang == 'es') {
                        $mesTraducido = "Enero";
                    } elseif ($my_current_lang == 'en') {
                        $mesTraducido = "January";
                    }
                    break;
                case 'Februaryy':
                    $my_current_lang = apply_filters( 'wpml_current_language', NULL );
                    if ($my_current_lang == 'es') {
                        $mesTraducido = "Febrero";
                    } elseif ($my_current_lang == 'en') {
                        $mesTraducido = "February";
                    }
                    break;
                case 'March':
                    $my_current_lang = apply_filters( 'wpml_current_language', NULL );
                    if ($my_current_lang == 'es') {
                        $mesTraducido = "Marzo";
                    } elseif ($my_current_lang == 'en') {
                        $mesTraducido = "March";
                    }
                    break;
                case 'April':
                    $my_current_lang = apply_filters( 'wpml_current_language', NULL );
                    if ($my_current_lang == 'es') {
                        $mesTraducido = "Abril";
                    } elseif ($my_current_lang == 'en') {
                        $mesTraducido = "April";
                    }
                    break;
                case 'May':
                    $my_current_lang = apply_filters( 'wpml_current_language', NULL );
                    if ($my_current_lang == 'es') {
                        $mesTraducido = "Mayo";
                    } elseif ($my_current_lang == 'en') {
                        $mesTraducido = "May";
                    }
                    break;
                case 'June':
                    $my_current_lang = apply_filters( 'wpml_current_language', NULL );
                    if ($my_current_lang == 'es') {
                        $mesTraducido = "Junio";
                    } elseif ($my_current_lang == 'en') {
                        $mesTraducido = "June";
                    }
                    break;
                case 'July':
                    $my_current_lang = apply_filters( 'wpml_current_language', NULL );
                    if ($my_current_lang == 'es') {
                        $mesTraducido = "Julio";
                    } elseif ($my_current_lang == 'en') {
                        $mesTraducido = "July";
                    }
                    break;
                case 'Agost':
                    $my_current_lang = apply_filters( 'wpml_current_language', NULL );
                    if ($my_current_lang == 'es') {
                        $mesTraducido = "Agosto";
                    } elseif ($my_current_lang == 'en') {
                        $mesTraducido = "Agost";
                    }
                    break;
                case 'September':
                    $my_current_lang = apply_filters( 'wpml_current_language', NULL );
                    if ($my_current_lang == 'es') {
                        $mesTraducido = "Septiembre";
                    } elseif ($my_current_lang == 'en') {
                        $mesTraducido = "September";
                    }
                    break;
                case 'October':
                    $my_current_lang = apply_filters( 'wpml_current_language', NULL );
                    if ($my_current_lang == 'es') {
                        $mesTraducido = "Octubre";
                    } elseif ($my_current_lang == 'en') {
                        $mesTraducido = "October";
                    }
                    break;
                case 'November':
                    $my_current_lang = apply_filters( 'wpml_current_language', NULL );
                    if ($my_current_lang == 'es') {
                        $mesTraducido = "Noviembre";
                    } elseif ($my_current_lang == 'en') {
                        $mesTraducido = "November";
                    }
                    break;
                case 'December':
                    $my_current_lang = apply_filters( 'wpml_current_language', NULL );
                    if ($my_current_lang == 'es') {
                        $mesTraducido = "Diciembre";
                    } elseif ($my_current_lang == 'en') {
                        $mesTraducido = "December";
                    }
                    break;
            }
            return $mesTraducido;
        }

        $output2 = '';
        foreach ($random_posts as $post) {
            $output2 .= '<div class="col-12 col-sm-6 col-md-4 p-2 art"><a href="'. get_permalink($post->ID) .'" role="article" style="color: #f00"><div class="articulo"><div  class="capaTexto"><header><div class="tipoEvento"></div>';

                if( have_rows('fecha_evento', $post->ID )):

                    // loop through the rows of data

                    while ( have_rows('fecha_evento', $post->ID)) : the_row();
                        $output = '';
                        $diaEvento = '';
                        $horaEvento = '';
                        $mesEvento = '';

                        $varDia = get_sub_field('dia_evento');
                        $diaEvento = date("d", strtotime($varDia));
                        $diaEvento .= '<br />';
                        $mesEvento1 = date('F', strtotime($varDia));
                        $mesEvento = cambiarMes($mesEvento1);
                       /* $horaEvento .= '<div class="horaEvento">';
                        $horaEvento .= ' de ';
                        $horaEvento .= get_sub_field('hora_comienzo');
                        $horaEvento .= ' a ';
                        $horaEvento .= get_sub_field('hora_final');
                        $horaEvento .= '</div>'; */
                        $output .= '<div class="diaEvento">'. $diaEvento .'</div>';
                        $output .= '<div class="mesEvento">'. $mesEvento .'</div>';
                        $diaEvento = '';
                        $horaEvento .= '<div class="horaEvento">'. get_sub_field('horarioEvento') .'</div>';
                        $output .= $horaEvento;

                    endwhile;

                endif;

            $imagenFondoEvento = get_field('imagen_repo',$post->ID);

            $lugares = get_field('lugar_evento', $post->ID);
            $pasarLugares = '';

            if($lugares) {
                $pasarLugares = $lugares[0]->post_title;
            } else {
                $pasarLugares = 'Todos los Espacios';
            }


            $output2 .= $output;
            $output2 .= ' <h2 class="entry-title" style="margin: 9% 0;"> '. get_the_title( $post->ID ).'</h2>';
            $output2 .= '</header><div class="entry-content">'. get_field('texto_introduccion',$post->ID). '</div><div class="linkLugarEvento">'.  $pasarLugares .'</div></div><div class="fondo" style="background-image: url('. esc_url($imagenFondoEvento['url']) .')"></div></div></a></div>';
        }


        return  $output2;
    }




    public function botonVerEventos () {
        $contTotal = '';
        $textLink = get_field('texto_link_eventos','option');
        $linkEventos = get_field('link_evento','option');
        $contTotal .= '<div class="button-container"><a href="'. $linkEventos .'" class="view-more-button boton">';
        $contTotal .= $textLink;
        $contTotal .= '</a></div>';
        return $contTotal;

    }

    /* aqui comienza el desarrollo para la comunidad */
    public function tituloSeccionComunidad()
    {
        $output = get_field('titulo_seccion_comunidad', 'option');
        return $output;
    }
    public function verLogos()
    {
        $contTotal = '';

        if (have_rows('comunidad','option')) {
            while (have_rows('comunidad','option')){

                the_row();
                // vars
                $image = get_sub_field('imagen_empresa');
                $urlImagen = $image['url'];
                $contTotal .= '<div class="col-6 col-md-3 sinPadding"><img src="';
                $contTotal .= $urlImagen;
                $contTotal .= '" width="100%" alt="'. $image['name'] .'"></div>';

            }
        }


        return $contTotal;


    }

}
