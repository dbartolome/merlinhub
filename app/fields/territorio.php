<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$territorio = new FieldsBuilder('campos_territorios');

$territorio
    ->setLocation('post_type', '==', 'territorio');

$territorio


    ->addFlexibleContent('contenedores', [
        'instructions' => '',
        'required' => 0,
        'conditional_logic' => [],
        'wrapper' => [
            'width' => '',
            'class' => '',
            'id' => '',
        ],
        'button_label' => 'Add Row',
        'min' => '',
        'max' => '',
    ])
        ->addLayout('layout1', [
            'label' => 'Layout texto con titulo y contenido',
            'instructions' => 'Nombre de la tienda',
            'display' => 'row',
            'sub_fields' => [],
            'min' => '',
            'max' => '',
        ])
            ->addText('titulo_espacio', [
                'label' => 'Titulo para el espacio',
                'instructions' => 'Nombre de la tienda',
                'required' => 0,
                'wrapper' => [
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ],
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
            ])
            ->addWysiwyg('contenido_espacio', [
                'label' => 'WYSIWYG Field',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => [],
                'wrapper' => [
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ],
                'default_value' => '',
                'tabs' => 'all',
                'toolbar' => 'full',
                'media_upload' => 1,
                'delay' => 0,
            ])
            ->addRange('tamanoLayout', [
        'label' => 'Ancho del bloque',
        'instructions' => 'seleccionar de 20 - 100 aconsejable minimo de 20',
        'required' => 0,
        'conditional_logic' => [],
        'wrapper' => [
            'width' => '',
            'class' => '',
            'id' => '',
        ],
        'default_value' => '',
        'min' => '10',
        'max' => '100',
        'step' => '1',
        'prepend' => '',
        'append' => '',
    ])
        ->addLayout('layout2', [
                'label' => 'Layaout imagen ',
                'instructions' => 'Nombre de la tienda',
                'display' => 'row',
                'sub_fields' => [],
                'min' => '',
                'max' => '',
            ])
            ->addImage('imagen_bloque_dos', [
        'label' => 'Imagen para la el bloque layaout 1',
        'instructions' => '',
        'required' => 0,
        'return_format' => 'array',
        'preview_size' => 'medium',
        'library' => 'all',
        'min_width' => '',
        'min_height' => '',
        'min_size' => '',
        'max_width' => '',
        'max_height' => '',
        'max_size' => '',
        'mime_types' => '',
    ])
            ->addTrueFalse('imagen_fondo', [
                'label' => 'Fondo',
                'instructions' => 'Si - es background :: No - es imagen normal',
                'required' => 0,
                'conditional_logic' => [],
                'wrapper' => [
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ],
                'message' => '',
                'default_value' => 0,
                'ui' => 1,
                'ui_on_text' => 'Si',
                'ui_off_text' => 'No',
            ])
            ->addRange('tamanoLayoutDos', [
        'label' => 'Ancho del bloque',
        'instructions' => 'seleccionar de 20 - 100 aconsejable minimo de 20',
        'required' => 0,
        'conditional_logic' => [],
        'wrapper' => [
            'width' => '',
            'class' => '',
            'id' => '',
        ],
        'default_value' => '',
        'min' => '10',
        'max' => '100',
        'step' => '1',
        'prepend' => '',
        'append' => '',
    ])
        ->addLayout('layout3', [
                    'label' => 'Layaout Editor con Wysiwyg',
                    'instructions' => ' ',
                    'display' => 'row',
                    'sub_fields' => [],
                    'min' => '',
                    'max' => '',
                ])
            ->addWysiwyg('contenido_espacio', [
                    'label' => 'WYSIWYG Field',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => [],
                    'wrapper' => [
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ],
                    'default_value' => '',
                    'tabs' => 'all',
                    'toolbar' => 'full',
                    'media_upload' => 1,
                    'delay' => 0,
                ])
            ->addRange('tamanoLayoutTres', [
        'label' => 'Ancho del bloque',
        'instructions' => 'seleccionar de 20 - 100 aconsejable minimo de 20',
        'required' => 0,
        'conditional_logic' => [],
        'wrapper' => [
            'width' => '',
            'class' => '',
            'id' => '',
        ],
        'default_value' => '',
        'min' => '10',
        'max' => '100',
        'step' => '1',
        'prepend' => '',
        'append' => '',
    ])
        ->addLayout('layout4', [
                    'label' => 'Texto sin formato',
                    'instructions' => ' ',
                    'display' => 'row',
                    'sub_fields' => [],
                    'min' => '',
                    'max' => '',
                ])
            ->addTextarea('contenido_espacio', [
                'label' => 'Texto del Layout',
                'instructions' => '',
                'required' => 0,
                'wrapper' => [
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ],
                'default_value' => '',
                'placeholder' => '',
                'maxlength' => '',
                'rows' => '',
                'new_lines' => '',
            ])
            ->addRange('tamanoLayoutCuatro', [
                    'label' => 'Ancho del bloque',
                    'instructions' => 'seleccionar de 20 - 100 aconsejable minimo de 20',
                    'required' => 0,
                    'conditional_logic' => [],
                    'wrapper' => [
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ],
                    'default_value' => '',
                    'min' => '10',
                    'max' => '100',
                    'step' => '1',
                    'prepend' => '',
                    'append' => '',
            ])
        ->addLayout('layout5', [
                'label' => 'Separador Secciones',
                'instructions' => ' ',
                'display' => 'row',
                'sub_fields' => [],
                'min' => '',
                'max' => '',
            ])
            ->addRange('tamanoLayoutCinco', [
                    'label' => 'Ancho del bloque',
                    'instructions' => 'seleccionar de 20 - 100 aconsejable minimo de 20',
                    'required' => 0,
                    'conditional_logic' => [],
                    'wrapper' => [
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ],
                    'default_value' => '',
                    'min' => '1',
                    'max' => '30',
                    'step' => '1',
                    'prepend' => '',
                    'append' => '',
                ])
        ->addLayout('layout6', [
        'label' => 'Grid de 4 columnas',
        'instructions' => ' ',
        'display' => 'row',
        'sub_fields' => [],
        'min' => '',
        'max' => '',
    ])
            ->addRange('tamanoLayoutSeis', [
            'label' => 'Ancho del bloque',
            'instructions' => 'seleccionar de 20 - 100 aconsejable minimo de 20',
            'required' => 0,
            'conditional_logic' => [],
            'wrapper' => [
                'width' => '',
                'class' => '',
                'id' => '',
            ],
            'default_value' => '',
            'min' => '20',
            'max' => '100',
            'step' => '1',
            'prepend' => '',
            'append' => '',
        ])
            ->addRepeater('gridTerritorio', [
            'label' => 'Grid para composiciones de 4 columnas',
            'instructions' => 'Especifica aquí qué iconos de redes sociales se usarán y a qué destino apunta la URL de cada icono',
            'max' => 12,
            'min' => 0,
            'button_label' => 'Agregar una imagen y texto al grid',
        ])
                ->addTextarea('codigoSVG', [
                'label' => 'Script para el svg',
                'instructions' => '',
                'required' => 0,
                'wrapper' => [
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ],
                'default_value' => '',
                'placeholder' => '',
                'maxlength' => '',
                'rows' => '',
                'new_lines' => '',
            ])
                ->addImage('imagenGrid', [
                'label' => 'Imagen o icono pra el grid',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => [],
                'wrapper' => [
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ],
                'return_format' => 'array',
                'preview_size' => 'thumbnail',
                'library' => 'all',
                'min_width' => '',
                'min_height' => '',
                'min_size' => '',
                'max_width' => '',
                'max_height' => '',
                'max_size' => '',
                'mime_types' => '',
             ])
                ->addText('textGrid', [
                'label' => 'Texto para el grid',
                'instructions' => '',
                'required' => 0,
                'wrapper' => [
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ],
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
            ])
                ->endRepeater()
        ->addLayout('layout7', [
        'label' => 'Layaout Editor con Wysiwyg sin margenes',
        'instructions' => ' ',
        'display' => 'row',
        'sub_fields' => [],
        'min' => '',
        'max' => '',
    ])
            ->addWysiwyg('contenido_espacio', [
        'label' => 'WYSIWYG Field',
        'instructions' => '',
        'required' => 0,
        'conditional_logic' => [],
        'wrapper' => [
            'width' => '',
            'class' => '',
            'id' => '',
        ],
        'default_value' => '',
        'tabs' => 'all',
        'toolbar' => 'full',
        'media_upload' => 1,
        'delay' => 0,
    ])
            ->addRange('tamanoLayoutSiete', [
        'label' => 'Ancho del bloque',
        'instructions' => 'seleccionar de 20 - 100 aconsejable minimo de 20',
        'required' => 0,
        'conditional_logic' => [],
        'wrapper' => [
            'width' => '',
            'class' => '',
            'id' => '',
        ],
        'default_value' => '',
        'min' => '10',
        'max' => '100',
        'step' => '1',
        'prepend' => '',
        'append' => '',
    ])
        ->addLayout('layout8', [
        'label' => 'Grid de 4 columnas sin lineas interiores',
        'instructions' => ' ',
        'display' => 'row',
        'sub_fields' => [],
        'min' => '',
        'max' => '',
    ])
            ->addRange('tamanoLayoutOcho', [
        'label' => 'Ancho del bloque',
        'instructions' => 'seleccionar de 20 - 100 aconsejable minimo de 20',
        'required' => 0,
        'conditional_logic' => [],
        'wrapper' => [
            'width' => '',
            'class' => '',
            'id' => '',
        ],
        'default_value' => '',
        'min' => '20',
        'max' => '100',
        'step' => '1',
        'prepend' => '',
        'append' => '',
    ])
            ->addRepeater('gridTerritorioSinLineas', [
        'label' => 'Grid para composiciones de 4 columnas',
        'instructions' => 'Especifica aquí qué iconos de redes sociales se usarán y a qué destino apunta la URL de cada icono',
        'max' => 12,
        'min' => 0,
        'button_label' => 'Agregar una imagen y texto al grid',
    ])
                ->addImage('imagenGrid', [
            'label' => 'Imagen o icono pra el grid',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => [],
            'wrapper' => [
                'width' => '',
                'class' => '',
                'id' => '',
            ],
            'return_format' => 'array',
            'preview_size' => 'thumbnail',
            'library' => 'all',
            'min_width' => '',
            'min_height' => '',
            'min_size' => '',
            'max_width' => '',
            'max_height' => '',
            'max_size' => '',
            'mime_types' => '',
        ])
                ->addTextarea('codigoSVG', [
        'label' => 'Script para el svg',
        'instructions' => '',
        'required' => 0,
        'wrapper' => [
            'width' => '',
            'class' => '',
            'id' => '',
        ],
        'default_value' => '',
        'placeholder' => '',
        'maxlength' => '',
        'rows' => '',
        'new_lines' => '',
    ])
                ->addText('nomCapa', [
        'label' => 'Nombre de la capa',
        'instructions' => '',
        'required' => 0,
        'wrapper' => [
            'width' => '',
            'class' => '',
            'id' => '',
        ],
        'default_value' => '',
        'placeholder' => '',
        'maxlength' => '',
        'rows' => '',
        'new_lines' => '',
    ])
                ->addText('TitularGrid', [
            'label' => 'Titular para el grid para el grid',
            'instructions' => '',
            'required' => 0,
            'wrapper' => [
                'width' => '',
                'class' => '',
                'id' => '',
            ],
            'default_value' => '',
            'placeholder' => '',
            'prepend' => '',
            'append' => '',
            'maxlength' => '',
        ])
                ->addText('textGrid', [
            'label' => 'Texto para el grid',
            'instructions' => '',
            'required' => 0,
            'wrapper' => [
                'width' => '',
                'class' => '',
                'id' => '',
            ],
            'default_value' => '',
            'placeholder' => '',
            'prepend' => '',
            'append' => '',
            'maxlength' => '',
        ])
                ->endRepeater()
        ->addLayout('layout9', [
        'label' => 'Insertear mapa',
        'instructions' => ' ',
        'display' => 'row',
        'sub_fields' => [],
        'min' => '',
        'max' => '',
    ])
            ->addWysiwyg('contenido_espacio', [
        'label' => 'WYSIWYG Field',
        'instructions' => '',
        'required' => 0,
        'conditional_logic' => [],
        'wrapper' => [
            'width' => '',
            'class' => '',
            'id' => '',
        ],
        'default_value' => '',
        'tabs' => 'all',
        'toolbar' => 'full',
        'media_upload' => 1,
        'delay' => 0,
    ])
            ->addRange('tamanoLayoutNueve', [
        'label' => 'Ancho del bloque',
        'instructions' => 'seleccionar de 20 - 100 aconsejable minimo de 20',
        'required' => 0,
        'conditional_logic' => [],
        'wrapper' => [
            'width' => '',
            'class' => '',
            'id' => '',
        ],
        'default_value' => '',
        'min' => '10',
        'max' => '100',
        'step' => '1',
        'prepend' => '',
        'append' => '',
    ]);
return $territorio;

