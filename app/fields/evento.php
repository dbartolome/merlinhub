<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$evento = new FieldsBuilder('campos_eventos');

$evento
    ->setLocation('post_type', '==', 'evento');

$evento
    ->addImage('imagen_repo', [
        'label' => 'Imagen qeu se mostrar en el repositorio',
        'return_format' => 'array',
        'preview_size' => 'thumbnail',

    ])
    ->addTextarea('texto_introduccion', [
        'label' => 'Texto para la intro del repositorio',
    ])
    ->addPostObject('espacioEvento', [
        'label' => 'Selecciona el espacio del evento',
        'instructions' => '',
        'required' => 0,
        'conditional_logic' => [],
        'wrapper' => [
            'width' => '',
            'class' => '',
            'id' => '',
        ],
        'post_type' => ['espacio'],
        'taxonomy' => [],
        'allow_null' => 0,
        'multiple' => 0,
        'return_format' => 'object',
        'ui' => 1,
    ])

    ->addDatePicker('fechaEvento', [
        'label' => 'Fecha del evento',
        'instructions' => '',
        'required' => 0,
        'conditional_logic' => [],
        'wrapper' => [
            'width' => '',
            'class' => '',
            'id' => '',
        ],
        'display_format' => 'd/m/Y',
        'return_format' => 'm/d/Y',
        'first_day' => 1,
    ])


;
return $evento;
