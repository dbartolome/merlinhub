<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$streaming = new FieldsBuilder('campos_streaming');

$streaming
    ->setLocation('post_type', '==', 'streaming');

$streaming
    ->addImage('encabezadoStreaming', [
        'label' => 'Imagen para el encabezado del streaming',
    ])
    ->addText('tituloStreaming', [
        'label' => 'Nombre para el streaming del streaming',
    ])

    ->addText('fechaStreaming', [
        'label' => 'Fecha del streaming',
    ])

    ->addText('lugarStreaming', [
        'label' => 'Desde donde se emite el streaming',
    ])
    ->addTextarea('scriptIframe', [
        'label' => 'Script par el streaming',
        'new_lines' => '', // Possible values are 'wpautop', 'br', or ''.
    ])

    ->addImage('imagenStreaming', [
        'label' => 'Imagen sustituir por iframe',
    ])


    ->addText('tituloSeccion', [
        'label' => 'Titulo para la seccion de los ponentes',
    ])

    ->addRepeater('ponentes', [
        'label' => 'Listado de ponentes',

    ])
        ->addImage('fotoPonente', [
            'label' => 'Imagen para el ponente',
        ])
        ->addText('nombrePonente', [
            'label' => 'Nombre del Ponente',
        ])

        ->addText('puestoPonente', [
            'label' => 'Puesto del Ponente',
        ])
        ->addText('moderadorPonente', [
            'label' => 'Escribir si es moderador u otros cargos',
        ])


    ->endRepeater()

;
return $streaming;
