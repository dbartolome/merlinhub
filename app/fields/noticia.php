<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$noticia = new FieldsBuilder('noticia');

$noticia
    ->setLocation('post_type', '==', 'post');

$noticia
    ->addImage('noticia_header_img', [
        'label' => 'Imagen header',
        'instructions' => 'Imagen para el header de esta noticia. La imagen destacada se utiliza para la imagen de la portada de noticias. Esta es para el header de la noticia.',
        'return_format' => 'array',
        'preview_size' => 'medium',
        'library' => 'all',
    ]);
$noticia
    ->addFields(get_field_partial('partials.leermas'));

return $noticia;
