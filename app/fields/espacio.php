<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$espacio = new FieldsBuilder('campos_espacios');

$espacio
    ->setLocation('post_type', '==', 'espacio');

$espacio
    ->addTab('datos', ['placement' => 'left'])
        ->addText('direccion_espacio', [
            'label' => 'Direccion',
            'instructions' => '',
            'default_value' => '',
        ])
        ->addText('ciudad_espacio', [
            'label' => 'Ciudad',
            'instructions' => '',
            'default_value' => '',
        ])
    ->addText('mapa_espacio', [
        'label' => 'Incluir el shoirtcode del mapa para mostrar en la ficha',
        'instructions' => '',
        'default_value' => '',
    ])
    ->addTab('iconos', ['placement' => 'left'])
        ->addText('espacios_superficie', [
            'label' => 'Superficie',
            'instructions' => 'Superficie en metros cuadrados',
            'default_value' => '',
        ])
        ->addText('espacios_edificios', [
            'label' => 'Edificios',
            'instructions' => 'Número de edificios en este espacio',
        ])
        ->addText('espacios_personas', [
            'label' => 'Personas',
            'instructions' => 'Número de personas en el espacio',
        ])
        ->addText('espacios_empresas', [
            'label' => 'Empresas',
            'instructions' => 'Número de empresas en el espacio',
            'default_value' => '',
        ])
        ->addTrueFalse('espacios_loom', [
            'label' => 'Loom',
            'instructions' => 'Activar si hay espacios Loom',
            'default_value' => 0,
            'ui' => 1,
        ])
        ->addTrueFalse('espacios_flexspaces', [
            'label' => 'Flexspaces',
            'instructions' => 'Actvar para que aparezca la leyenda FLEXSPACES by LOOM',
            'default_value' => 0,
            'ui' => 1,
        ])
        ->conditional('espacios_loom', '==', '1')

       ->addTaxonomy('espacios_espacios_comunes', [
           'label' => 'Espacios comunes',
           'instructions' => '',
           'required' => 0,
           'conditional_logic' => [],
           'wrapper' => [
               'width' => '33%',
               'class' => '',
               'id' => '',
           ],
           'taxonomy' => 'espaciocomun',
           'field_type' => 'checkbox',
           'allow_null' => 0,
           'add_term' => 0,
           'save_terms' => 0,
           'load_terms' => 0,
           'return_format' => 'object',
           'multiple' => 1,
       ])
        ->addTaxonomy('espacios_restaurantes', [
        'label' => 'Espacios de Restauracion',
        'instructions' => '',
        'required' => 0,
        'conditional_logic' => [],
        'wrapper' => [
            'width' => '33%',
            'class' => '',
            'id' => '',
        ],
        'taxonomy' => 'serviciorestauracion',
        'field_type' => 'checkbox',
        'allow_null' => 0,
        'add_term' => 0,
        'save_terms' => 0,
        'load_terms' => 0,
        'return_format' => 'object',
        'multiple' => 1,
    ])
        ->addTaxonomy('espacios_servicios', [
        'label' => 'Servicios',
        'instructions' => '',
        'required' => 0,
        'conditional_logic' => [],
        'wrapper' => [
            'width' => '33%',
            'class' => '',
            'id' => '',
        ],
        'taxonomy' => 'servicio',
        'field_type' => 'checkbox',
        'return_format' => 'object',
        'multiple' => 1,
    ])
        ->addTaxonomy('espacios_wellness', [
        'label' => 'Espacios wellnes',
        'instructions' => '',
        'required' => 0,
        'conditional_logic' => [],
        'wrapper' => [
            'width' => '33%',
            'class' => '',
            'id' => '',
        ],
        'taxonomy' => 'espaciowellness',
        'field_type' => 'checkbox',
        'allow_null' => 0,
        'add_term' => 0,
        'save_terms' => 0,
        'load_terms' => 0,
        'return_format' => 'object',
        'multiple' => 1,
    ])
        ->addTaxonomy('espacios_comunidad', [
        'label' => 'Espacios de la comunidad',
        'instructions' => '',
        'required' => 0,
        'conditional_logic' => [],
        'wrapper' => [
            'width' => '33%',
            'class' => '',
            'id' => '',
        ],
        'taxonomy' => 'serviciocomunidad',
        'field_type' => 'checkbox',
        'allow_null' => 0,
        'add_term' => 0,
        'save_terms' => 0,
        'load_terms' => 0,
        'return_format' => 'object',
        'multiple' => 1,
    ])


    ->addTab('botones', ['placement' => 'left'])
        ->addUrl('espacios_microsite_url', [
            'label' => 'URL del microsite',
            'instructions' => 'Se utiliza para construir el botón microsite de la página',
            'placeholder' => 'https://microsite.com',
        ])
        ->addUrl('espacios_tour', [
            'label' => 'URL del tour',
            'instructions' => 'Se utiliza para construir el botón que enlaza al tour',
            'placeholder' => 'https://eltour.com',
        ])
    ->addTab('espacios_certificados_calidad', ['placement' => 'left'])
        ->addCheckbox('espacios_certificados', [
            'label' => 'Certificados',
            'instructions' => 'Especifica qué certificados de calidad tiene este espacio',
            'required' => 0,
            'choices' => [
                'leed' => 'Leed',
                'wired_score' => 'Wired Score',
                'diga' => 'Diga',
                'breeam' => 'Breeam',
            ],
            'allow_custom' => 0,
            'layout' => 'vertical',
            'toggle' => 1,
            'return_format' => 'value',
        ])
    ->addTab('galeria', ['placement' => 'left'])
        ->addRepeater('espacios_imagenes_galeria', [
            'label' => 'Imágenes galería',
            'instructions' => 'Añade imágenes a la galería de este espacio',
            'min' => 0,
            'max' => 0,
            'layout' => 'table',
            'button_label' => 'Añadir otra',
        ])
            ->addImage('espacios_imagen_galeria', [
                'label' => 'Imagen',
                'instructions' => 'Selecciona la imagen',
                'return_format' => 'array',
                'preview_size' => 'thumbnail',
                'library' => 'all',
                'min_width' => '',
                'min_height' => '',
                'max_width' => '',
                'max_height' => '',
            ])
        ->endRepeater()
;

return $espacio;
