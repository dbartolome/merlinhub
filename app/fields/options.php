<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

acf_add_options_page([
    'page_title' => get_bloginfo('name') . ' theme options',
    'menu_title' => 'Opciones del tema',
    'menu_slug'  => 'theme-options',
    'capability' => 'edit_theme_options',
    'position'   => '999',
    'autoload'   => true
]);

$options = new FieldsBuilder('theme_options');

$options
    ->setLocation('options_page', '==', 'theme-options');

$options
    ->addTab('Portada', ['placement' => 'left'])
        ->addImage('logo_peq', [
            'label' => 'Imagen para el logo en el menu lateral contraido',
            'instructions' => '',
            'required' => 0,
            'return_format' => 'array',
            'preview_size' => 'medium',
            'library' => 'all',
            'min_width' => '',
            'min_height' => '',
            'min_size' => '',
            'max_width' => '',
            'max_height' => '',
            'max_size' => '',
            'mime_types' => '',
        ])
        ->addImage('logo_gra', [
            'label' => 'Imagen para el logo en el menu lateral extendido',
            'instructions' => '',
            'required' => 0,
            'return_format' => 'array',
            'preview_size' => 'medium',
            'library' => 'all',
            'min_width' => '',
            'min_height' => '',
            'min_size' => '',
            'max_width' => '',
            'max_height' => '',
            'max_size' => '',
            'mime_types' => '',
        ])
        ->addRepeater('social', [
            'label' => 'Iconos de redes sociales',
            'instructions' => 'Especifica aquí qué iconos de redes sociales se usarán y a qué destino apunta la URL de cada icono',
            'max' => 4,
            'min' => 0,
            'button_label' => 'Añadir un frame',
        ])
        ->addText('social_url', [
        'label' => 'URL',
        'instructions' => 'Especifica aquí la URL de la red social a la que apunta el enlace del icon',
    ])
        ->addImage('social_icon', [
        'label' => 'Imagen para las redes sociales',
        'instructions' => '',
        'required' => 0,
        'return_format' => 'array',
        'preview_size' => 'medium',
        'library' => 'all',
        'min_width' => '',
        'min_height' => '',
        'min_size' => '',
        'max_width' => '',
        'max_height' => '',
        'max_size' => '',
        'mime_types' => '',
    ])
        ->endRepeater()
    ->addTab('Header Home', ['placement' => 'left'])
        ->addText('NombreSlider', [
            'label' => 'Text Field',
            'instructions' => '',
        ])
    /* Tab opciones Destacado Uno*/
    ->addTab('Destacado Uno', ['placement' => 'left'])
        ->addImage('fondo_destacado', [
            'label' => 'Imagen para el fondo del destacado',
            'instructions' => '',
            'required' => 0,
            'return_format' => 'array',
            'preview_size' => 'medium',
            'library' => 'all',
            'min_width' => '',
            'min_height' => '',
            'min_size' => '',
            'max_width' => '',
            'max_height' => '',
            'max_size' => '',
            'mime_types' => '',
        ])
        ->addText('titular_destacado', [
            'label' => 'Titular del destacado',
            'instructions' => '',
            'required' => 0,
            'wrapper' => [
                'width' => '',
                'class' => '',
                'id' => '',
            ],
            'default_value' => '',
            'placeholder' => '',
            'prepend' => '',
            'append' => '',
            'maxlength' => '',
        ])
        ->addTextarea('texto_destacado', [
            'label' => 'Texto del destacado',
            'instructions' => '',
            'required' => 0,
            'wrapper' => [
                'width' => '',
                'class' => '',
                'id' => '',
            ],
            'default_value' => '',
            'placeholder' => '',
            'maxlength' => '',
            'rows' => '',
            'new_lines' => '',
        ])
        ->addPageLink('enlace_destacado', [
            'label' => 'Enlace de la Noticia',
            'type' => 'page_link',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => [],
            'wrapper' => [
                'width' => '',
                'class' => '',
                'id' => '',
            ],
            'post_type' => [],
            'taxonomy' => [],
            'allow_null' => 0,
            'allow_archives' => 1,
            'multiple' => 0,
        ])
        ->addText('texto_enlace_destacado', [
        'label' => 'Texto para el enlace del destacado',
        'instructions' => '',
        'required' => 0,
        'wrapper' => [
            'width' => '',
            'class' => '',
            'id' => '',
        ],
        'default_value' => '',
        'placeholder' => '',
        'prepend' => '',
        'append' => '',
        'maxlength' => '',
    ])
    /* Tab opciones Programa APP */
    ->addTab('Programa APP', ['placement' => 'left'])
        ->addRepeater('destacadoDos', [
            'label' => 'Filas para el Segundo grupo de destacados',
            'instructions' => 'Especifica aquí qué iconos de redes sociales se usarán y a qué destino apunta la URL de cada icono',
            'max' => 6,
            'min' => 0,
            'layout' => 'block',
            'button_label' => 'Añadir otro destacado',
        ])
            ->addImage('imagen_programa', [
                'label' => 'Imagen para el programa',
                'instructions' => '',
                'required' => 0,
                'return_format' => 'array',
                'preview_size' => 'medium',
                'library' => 'all',
                'min_width' => '',
                'min_height' => '',
                'min_size' => '',
                'max_width' => '',
                'max_height' => '',
                'max_size' => '',
                'mime_types' => '',
            ])
            ->addText('titular_programa', [
                'label' => 'Titular del programa',
                'instructions' => '',
                'required' => 0,
                'wrapper' => [
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ],
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
            ])
            ->addTextarea('texto_programa', [
                'label' => 'Texto del programa',
                'instructions' => '',
                'required' => 0,
                'wrapper' => [
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ],
                'default_value' => '',
                'placeholder' => '',
                'maxlength' => '',
                'rows' => '',
                'new_lines' => '',
            ])
            ->addPageLink('enlace_programa', [
                'label' => 'Enlace del programa',
                'type' => 'page_link',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => [],
                'wrapper' => [
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ],
                'post_type' => [],
                'taxonomy' => [],
                'allow_null' => 0,
                'allow_archives' => 1,
                'multiple' => 0,
            ])
            ->addText('texto_enlace_programa', [
        'label' => 'Texto para el enlace del programa',
        'instructions' => '',
        'required' => 0,
        'wrapper' => [
            'width' => '',
            'class' => '',
            'id' => '',
        ],
        'default_value' => '',
        'placeholder' => '',
        'prepend' => '',
        'append' => '',
        'maxlength' => '',
    ])
        ->endRepeater()
    /* Tab opciones Territorios*/
    ->addTab('Territorios', ['placement' => 'left'])
        ->addRepeater('territorio', [
            'label' => 'Filas para los territorios',
            'instructions' => 'Especifica aquí qué iconos de redes sociales se usarán y a qué destino apunta la URL de cada icono',
            'max' => 6,
            'min' => 0,
            'layout' => 'block',
            'button_label' => 'Añadir territorio',
        ])
        ->addImage('imagen_territorio', [
            'label' => 'Imagen para el fondo del territorio',
            'instructions' => '',
            'required' => 0,
            'return_format' => 'array',
            'preview_size' => 'medium',
            'library' => 'all',
            'min_width' => '',
            'min_height' => '',
            'min_size' => '',
            'max_width' => '',
            'max_height' => '',
            'max_size' => '',
            'mime_types' => '',
        ])
        ->addText('titular_territorio', [
            'label' => 'Titular del territorio',
            'instructions' => '',
            'required' => 0,
            'wrapper' => [
                'width' => '',
                'class' => '',
                'id' => '',
            ],
            'default_value' => '',
            'placeholder' => '',
            'prepend' => '',
            'append' => '',
            'maxlength' => '',
        ])
        ->addTextarea('texto_territorio', [
            'label' => 'Texto del territorio',
            'instructions' => '',
            'required' => 0,
            'wrapper' => [
                'width' => '',
                'class' => '',
                'id' => '',
            ],
            'default_value' => '',
            'placeholder' => '',
            'maxlength' => '',
            'rows' => '',
            'new_lines' => '',
        ])
        ->addPageLink('enlace_territorio', [
            'label' => 'Enlace de la territorio',
            'type' => 'page_link',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => [],
            'wrapper' => [
                'width' => '',
                'class' => '',
                'id' => '',
            ],
            'post_type' => [],
            'taxonomy' => [],
            'allow_null' => 0,
            'allow_archives' => 1,
            'multiple' => 0,
        ])
        ->addText('texto_enlace_territorio', [
            'label' => 'Texto para el enlace del territorio',
            'instructions' => '',
            'required' => 0,
            'wrapper' => [
                'width' => '',
                'class' => '',
                'id' => '',
            ],
            'default_value' => '',
            'placeholder' => '',
            'prepend' => '',
            'append' => '',
            'maxlength' => '',
        ])
        ->addRadio('posicionimagen', [
            'label' => 'Posicion de la imagen en el grid de territorios',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => [],
            'wrapper' => [
                'width' => '',
                'class' => '',
                'id' => '',
            ],
            'choices' => ['derecha' , 'izquierda'],
            'allow_null' => 0,
            'other_choice' => 0,
            'save_other_choice' => 0,
            'default_value' => 'derecha',
            'layout' => 'vertical',
            'return_format' => 'value',
        ])
        ->endRepeater()
    /* Tab opciones Explora Merlin hub*/
    ->addTab('Explora Merlin Hub', ['placement' => 'left'])
        ->addText('titulo_seccion_explora', [
            'label' => 'Titulo para la seccion de exploraMerlinhub',
            'instructions' => '',
            'required' => 0,
            'wrapper' => [
                'width' => '',
                'class' => '',
                'id' => '',
            ],
            'default_value' => '',
            'placeholder' => '',
            'prepend' => '',
            'append' => '',
            'maxlength' => '',
        ])
    ->addTextarea('codigo-Explora', [
        'label' => 'Codigo para el svg de explora',
        'instructions' => '',
        'required' => 0,
        'wrapper' => [
            'width' => '',
            'class' => '',
            'id' => '',
        ],
        'default_value' => '',
        'placeholder' => '',
        'maxlength' => '',
        'rows' => '',
        'new_lines' => 'No formatting',
    ])
    ->addText('texto_link_explorar', [
        'label' => 'Texto del boton de ver en el mapa',
        'instructions' => '',
        'required' => 0,
        'wrapper' => [
            'width' => '',
            'class' => '',
            'id' => '',
        ],
    ])
    ->addPageLink('link_explorar', [
        'label' => 'Enlace a la pagina de mapa de espacios',
        'type' => 'page_link',
        'instructions' => '',
        'required' => 0,
        'conditional_logic' => [],
        'wrapper' => [
            'width' => '',
            'class' => '',
            'id' => '',
        ],
        'post_type' => [],
        'taxonomy' => [],
        'allow_null' => 0,
        'allow_archives' => 1,
        'multiple' => 0,
    ])

    /* Tab opciones Noticias*/
    ->addTab('Noticia', ['placement' => 'left'])
        ->addText('titulo_seccion_noticia', [
            'label' => 'Titulo para la seccion de exploraMerlinhub',
            'instructions' => '',
            'required' => 0,
            'wrapper' => [
                'width' => '',
                'class' => '',
                'id' => '',
            ],
            'default_value' => '',
            'placeholder' => '',
            'prepend' => '',
            'append' => '',
            'maxlength' => '',
        ])
        ->addImage('imagen_noticia', [
            'label' => 'Imagen para la noticia',
            'instructions' => '',
            'required' => 0,
            'return_format' => 'array',
            'preview_size' => 'medium',
            'library' => 'all',
            'min_width' => '',
            'min_height' => '',
            'min_size' => '',
            'max_width' => '',
            'max_height' => '',
            'max_size' => '',
            'mime_types' => '',
        ])
        ->addText('titular_noticia', [
            'label' => 'Titular de la noticia',
            'instructions' => '',
            'required' => 0,
            'wrapper' => [
                'width' => '',
                'class' => '',
                'id' => '',
            ],
            'default_value' => '',
            'placeholder' => '',
            'prepend' => '',
            'append' => '',
            'maxlength' => '',
        ])
        ->addTextarea('txt_noticia', [
            'label' => 'Texto de la noticia',
            'instructions' => '',
            'required' => 0,
            'wrapper' => [
                'width' => '',
                'class' => '',
                'id' => '',
            ],
            'default_value' => '',
            'placeholder' => '',
            'maxlength' => '',
            'rows' => '',
            'new_lines' => '',
        ])
        ->addPageLink('enlace_noticia', [
            'label' => 'Enlace de la Noticia',
            'type' => 'page_link',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => [],
            'wrapper' => [
                'width' => '',
                'class' => '',
                'id' => '',
            ],
            'post_type' => [],
            'taxonomy' => [],
            'allow_null' => 0,
            'allow_archives' => 1,
            'multiple' => 0,
        ])
        ->addText('texto_enlace_noticia', [
        'label' => 'Texto para el enlace de la noticia',
        'instructions' => '',
        'required' => 0,
        'wrapper' => [
            'width' => '',
            'class' => '',
            'id' => '',
        ],
        'default_value' => '',
        'placeholder' => '',
        'prepend' => '',
        'append' => '',
        'maxlength' => '',
    ])
    /* Tab opciones Agenda*/
    ->addTab('Eventos', ['placement' => 'left'])
        ->addText('titulo_seccion_agenda', [
            'label' => 'Titulo para la seccion de agenda',
            'instructions' => '',
            'required' => 0,
            'wrapper' => [
                'width' => '',
                'class' => '',
                'id' => '',
            ],
            'default_value' => '',
            'placeholder' => '',
            'prepend' => '',
            'append' => '',
            'maxlength' => '',
        ])
        ->addRelationship('eventosHome', [
            'label' => 'Eventos para mostrar',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => [],
            'wrapper' => [
                'width' => '',
                'class' => '',
                'id' => '',
            ],
            'post_type' => ['evento'],
            'taxonomy' => [],
            'filters' => [
            ],
            'elements' => '',
            'min' => '',
            'max' => '3',
            'return_format' => 'object',
        ])

        ->addText('texto_link_eventos', [
            'label' => 'Texto del boton de ver mas eventos',
            'instructions' => '',
            'required' => 0,
            'wrapper' => [
                'width' => '',
                'class' => '',
                'id' => '',
            ],
        ])
        ->addPageLink('link_evento', [
            'label' => 'Enlace a la pagina de eventos',
            'type' => 'page_link',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => [],
            'wrapper' => [
                'width' => '',
                'class' => '',
                'id' => '',
            ],
            'post_type' => [],
            'taxonomy' => [],
            'allow_null' => 0,
            'allow_archives' => 1,
            'multiple' => 0,
        ])

    /* Tab opciones Comunidad */
    ->addTab('Comunidad', ['placement' => 'left'])
        ->addText('titulo_seccion_comunidad', [
            'label' => 'Titulo para la seccion de comunidad',
            'instructions' => '',
            'required' => 0,
            'wrapper' => [
                'width' => '',
                'class' => '',
                'id' => '',
            ],
            'default_value' => '',
            'placeholder' => '',
            'prepend' => '',
            'append' => '',
            'maxlength' => '',
        ])
        ->addRepeater('comunidad', [
            'label' => 'Recuadros para al acomunidad de empresas',
            'instructions' => '',
            'max' => 0,
            'min' => 0,
            'layout' => 'block',
            'button_label' => 'Añadir logo empresa',
        ])
        ->addImage('imagen_empresa', [
            'label' => 'Imagen para la Imagen de la comunidad',
            'instructions' => '',
            'required' => 0,
            'return_format' => 'array',
            'preview_size' => 'medium',
            'library' => 'all',
            'min_width' => '',
            'min_height' => '',
            'min_size' => '',
            'max_width' => '',
            'max_height' => '',
            'max_size' => '',
            'mime_types' => '',
        ])
        ->endRepeater()
    ->addTab('Header para agenda', ['placement' => 'left'])
    ->addImage('img_header', [
        'label' => 'Imagen para el logo en el menu lateral contraido',
        'instructions' => '',
        'required' => 0,
        'return_format' => 'array',
        'preview_size' => 'medium',
        'library' => 'all',
        'min_width' => '',
        'min_height' => '',
        'min_size' => '',
        'max_width' => '',
        'max_height' => '',
        'max_size' => '',
        'mime_types' => '',
    ])
    ->addTab('Footer', ['placement' => 'left'])
    ->addImage('logo_footer', [
        'label' => 'Logo Footer',
        'instructions' => '',
        'required' => 0,
        'return_format' => 'array',
        'preview_size' => 'medium',
        'library' => 'all',
        'min_width' => '',
        'min_height' => '',
        'min_size' => '',
        'max_width' => '',
        'max_height' => '',
        'max_size' => '',
        'mime_types' => '',
    ])
->addTab('Repositorio eventos', ['placement' => 'left'])
    ->addText('titSeccionEventosRepo', [
        'label' => 'Titulo Para seccion repo eventos',
        'instructions' => '',
    ])
    ->addWysiwyg('introSeccionEventosRepo', [
        'label' => 'Texto Para repo eventos',
        'instructions' => '',
    ]);

return $options;
