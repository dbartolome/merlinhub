<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$leermas = new FieldsBuilder('leer-mas');

$leermas
    ->addTrueFalse('leer_mas', [
        'label' => 'Añadir texto Leer Más',
        'message' => 'Añadir',
        'default_value' => 0,
        'ui' => 1,
        'ui_on_text' => '',
        'ui_off_text' => '',
    ]);

return $leermas;

