<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$eneec = new FieldsBuilder('eneec');

$eneec
    ->setLocation('post_template', '==', 'views/el-norte-es-el-centro.blade.php');

$eneec
    ->addWysiwyg('eneec_txt1', [
        'label' => 'Texto 1',
        'tabs' => 'all',
        'toolbar' => 'basic',
        'media_upload' => 0,
    ])
    ->addImage('eneec_mapa1', [
        'label' => 'Mapa 1',
        'return_format' => 'array',
        'preview_size' => 'medium',
        'library' => 'all',
    ])
    ->addWysiwyg('eneec_txt1b', [
        'label' => 'Texto 1b',
        'tabs' => 'all',
        'toolbar' => 'basic',
        'media_upload' => 0,
    ])
    ->addWysiwyg('eneec_txt2', [
        'label' => 'Texto 2',
        'tabs' => 'all',
        'toolbar' => 'full',
        'media_upload' => 0,
    ])
    ->addWysiwyg('eneec_piedemapa', [
        'label' => 'Pie del mapa',
        'tabs' => 'all',
        'toolbar' => 'basic',
        'media_upload' => 0,
    ])
    ->addImage('eneec_mapa2', [
        'label' => 'Mapa 2',
        'return_format' => 'array',
        'preview_size' => 'medium',
        'library' => 'all',
    ])
    ->addWysiwyg('eneec_txt3', [
        'label' => 'Texto 3',
        'tabs' => 'all',
        'toolbar' => 'full',
        'media_upload' => 0,
    ])
    ->addImage('eneec_img1', [
        'label' => 'Imagen 1',
        'return_format' => 'array',
        'preview_size' => 'medium',
        'library' => 'all',
    ])
    ->addWysiwyg('eneec_txt4', [
        'label' => 'Texto 4',
        'tabs' => 'all',
        'toolbar' => 'full',
        'media_upload' => 0,
    ])
    ->addImage('eneec_img2', [
        'label' => 'Imagen para texto 4',
        'return_format' => 'array',
        'preview_size' => 'medium',
        'library' => 'all',
    ])
    ->addWysiwyg('eneec_txt5', [
        'label' => 'Texto 5',
        'tabs' => 'all',
        'toolbar' => 'full',
        'media_upload' => 0,
    ])
    ->addImage('eneec_img3', [
        'label' => 'Imagen para texto 5',
        'return_format' => 'array',
        'preview_size' => 'medium',
        'library' => 'all',
    ]);


return $eneec;

