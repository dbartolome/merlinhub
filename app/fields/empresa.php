<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$empresa = new FieldsBuilder('campos_empresas');

$empresa
    ->setLocation('post_type', '==', 'empresa');

$empresa
    ->addUrl('enlaceEmpresa', [
        'label' => 'Enlace o Link empresa',
        'instructions' => '',
        'required' => 0,
        'conditional_logic' => [],
        'wrapper' => [
            'width' => '',
            'class' => '',
            'id' => '',
        ],
        'default_value' => '',
        'placeholder' => '',
    ])
    ->addRelationship('espacioEmpresa', [
        'label' => 'Espacio al qeu pertenece',
        'instructions' => '',
        'required' => 0,
        'conditional_logic' => [],
        'wrapper' => [
            'width' => '',
            'class' => '',
            'id' => '',
        ],
        'post_type' => ['espacio'],
        'taxonomy' => [],
        'filters' => [
            0 => 'search',
        ],
        'elements' => '',
        'min' => '',
        'max' => '1',
        'return_format' => 'ID',
    ]);

return $empresa;


