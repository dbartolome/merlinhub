<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$hub = new FieldsBuilder('campos_hub');

$hub
    ->setLocation('post_type', '==', 'hub');

$hub


    ->addText('nombre', [
        'label' => 'Nombre',
        'instructions' => 'Nombre de la tienda',
        'required' => 0,
        'wrapper' => [
            'width' => '',
            'class' => '',
            'id' => '',
        ],
        'default_value' => '',
        'placeholder' => '',
        'prepend' => '',
        'append' => '',
        'maxlength' => '',
    ]);

return $hub;

