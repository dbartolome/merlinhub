<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$page = get_page_by_path('merlin-hub-app-o-como-sacarle-todoel-partido-a-merlin-hub', 'ARRAY_N');

$iconos_mh_app = new FieldsBuilder('iconos_mh_app');

$iconos_mh_app
    ->setLocation('post_type', '==', 'page')
    ->and('page_template', '==', 'views/merlin-hub-app.blade.php');

$iconos_mh_app
->addTab('Iconos', ['placement' => 'left'])
    ->addRepeater('mhapp_bloque', [
        'label' => 'Bloques icono/texto',
        'button_label' => 'Añadir bloque',
    ])
        ->addRadio('mhapp_icono', [
            'label' => 'Icono',
            'instructions' => 'Icono del bloque',
            'choices' => [
                'control' => 'Control de accesos',
                'movilidad' => 'Movilidad',
                'ofertas' => 'Ofertas y descuentos',
                'food' => 'Food and drink',
                'noticias' => 'Noticias',
                'transporte' => 'Transporte a la demanda',
            ],
            'layout' => 'vertical',
            'return_format' => 'value',
        ])
        ->addWysiwyg('mhapp_texto', [
            'label' => 'Contenido de texto',
            'instructions' => '',
            'tabs' => 'all',
            'toolbar' => 'full',
            'media_upload' => 0,
        ])
    ->endRepeater()
->addTab('Badges', ['placement' => 'left'])
    ->addUrl('mhapp_googleplay', [
        'label' => 'Google Play',
        'instructions' => 'URL hacia la aplicación en Google Play',
    ])
    ->addUrl('mhapp_applestore', [
        'label' => 'Apple Store',
        'instructions' => 'URL hacia la aplicación en Apple Store',
    ])
;

return $iconos_mh_app;
